# DiscordBotCS

This is a bot for the Discord messaging app. I do not reccomend using this yourself, as I created it mainly for my use only.

Currently the bot can display a random meme, evaluate simple math expressions, render LaTeX math equations and manage users to an extent.
More is planned for the future, and I plan on adding advanced Reddit and Steam integration.

The branch labeled Release-0.1 contains the code for the initial release of the software. It is considerably more limited than what I've got on the dev branch.

Initially, the bot created user profiles for each user on the servers it was added to. So pretty much, if it's added to a server with 4,000 users, it will create 4,000 new user accounts after being added.
In addition, user accounts and other data was stored on the disk using Google's Protobuf. So, it was a pretty non-portable system.

The new branch can interface with an SQL server, plus it's much much more configurable.

In the future I aim to reduce the number of external dependencies. 
As of right now, the bot depends on a huge number of libraries, some of which aren't even maintained (scary, right!).