﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*

TODO: finish implementation of this

*/
namespace DiscordBotCS.Util
{
	/*
	 * The operation one item is applying to the next
	 * Note: only three operations here as division is handled via fractions
	 * Note: None is only used when there is no following parameter, however, it's not necessary either
	 */ 
	public enum EOperation
	{
		None = 0,
		Add,
		Sub,
		Mul,
	}
	/*
	 * Core interface for expressions, both simple and complex
	 */ 
	public interface IExpression
	{ }

	/*
	 * Core interface for expression components
	 */ 
	public interface IExpressionComponent
	{

	}

	/*
	 * A non-simple expression. Everything in math can be expressed as a fraction, so there's a numerator and divisor here
	 */ 
	public class ComplexExpression : IExpression
	{
		public IExpression Numerator;
		public IExpression Divisor;
	}

	/*
	 * A simplistic expression, does not have a divisor. Technically does, but this will ALWAYS be 1, and therefore, not exist
	 */ 
	public class SimpleExpression : IExpression
	{
		/*
		 * Components have operations applied to them in a linear direction
		 */ 
		IExpressionComponent[] Components;
	}

	public class Function : IExpressionComponent
	{
		IExpressionComponent[] Operands;
	}

	/*
	 * TODO: do this in a more memory-efficient way
	 */ 
	public class Number : IExpressionComponent
	{
		public double Value;
	}

	/*
	 * TODO: do this in a more memory-efficient way
	 */ 
	public class Constant : IExpressionComponent
	{
		public double Value;
	}

	

	/*
	 * For string parsing, function ids.
	 */ 
	public struct Functions
	{
		public static string Ln = "ln";
		public static string Log = "log";
		public static string Cosine = "cos";
		public static string Sine = "sin";
		public static string Tangent = "tan";
		public static string Secant = "sec";
		public static string CoSecant = "csc";
		public static string CoTangent = "cot";
		public static string ArcSine = "arcsine";
		public static string ArcCosine = "arccos";
		public static string ArcTangent = "arctan";
		public static string Tanh = "tanh";
		public static string Sinh = "sinh";
		public static string Cosh = "cosh";
		public static string Sech = "sech";
		public static string Csch = "csch";
		public static string Coth = "coth";
		public static string ArcTanh = "arctanh";
		public static string ArcCosh = "arccosh";
		public static string ArcSinh = "arcsinh";
		public static string ArcSech = "arcsech";
		public static string ArcCsch = "arccsch";
		public static string ArcCoth = "arccoth";
		public static string PI = "pi";
		public static string NaturalLogBase = "e";
		public static string AbsoluteValue = "|";
		public static string SquareRoot = "sqrt";
		public static string Root = "root";
		public static string Power = "pow";
		public static string Factorial = "!";
	}

	/*
	 * For string parsing, maps to constants
	 */ 
	public struct Constants
	{
		public static string PI = "pi";
	}
	/*
	 * Contains utils for solving mathematical expressions
	 * Pretty basic, not something you'd normally use in an actual
	 * quality application
	 */ 
	public static class ExpressionSolver
	{

	}
}
