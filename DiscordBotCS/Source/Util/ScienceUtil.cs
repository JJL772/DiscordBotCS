﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Util
{
	public class Element
	{
		public float AtomicMass { get; private set; }
		public int AtomicNumber { get; private set; }
		public string ElementSymbol { get; private set; }
		public string ElementName { get; private set; }
		public float ElectroNegativity { get; private set; }
		public float ElectronAffinity { get; private set; }

		public Element(string sym, string name, int number, float mass, float electroNegativity, float electronAffinity)
		{
			AtomicMass = mass;
			ElementSymbol = sym;
			ElementName = name;
			AtomicMass = mass;
			ElectroNegativity = electroNegativity;
			ElectronAffinity = electronAffinity;
		}
	}
}
