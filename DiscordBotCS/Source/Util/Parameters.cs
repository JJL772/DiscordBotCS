﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Util
{
	public class Parameter
	{
		public Parameter(EType type, EValueType valtype, string name, string delim, string help, bool opt = false)
		{
			Type = type;
			ValueType = valtype;
			Name = name;
			Delim = delim;
			HelpText = help;
			Optional = opt;
		}

		public enum EType
		{
			NoValue,
			SingleValue,
			List,
		}

		public EType Type;

		public bool Optional;

		public string Name;

		public string Delim;

		public enum EValueType
		{
			Int,
			Float,
			Bool,
			String,
		}

		public EValueType ValueType;

		public object Value = null;

		//Set to false if the value was incorrect
		//public bool CorrectVal = true;

		public string HelpText;

		public string GetFullName()
		{
			return Delim + Name;
		}
	}
	public static class Params
	{
		/// <summary>
		/// 
		/// </summary>
		/// <param name="paramdesc"></param>
		/// <param name="param"></param>
		public static void Parse(ref List<Parameter> paramdesc, string [] param)
		{
			//Start to look through the parameter list
			for(int i = 0; i < param.Length; i++)
			{
				//Determine if an element of the list contains a valid parameter string
				foreach(Parameter parm in paramdesc)
				{
					//Returns true if the list entry starts with a parameter string
					if(param[i].StartsWith(parm.GetFullName()))
					{
						switch(parm.Type)
						{
							//No value for the paremter
							case Parameter.EType.NoValue:
								parm.Value = new object();
								//parm.CorrectVal = true;
								break;
							case Parameter.EType.SingleValue:
								if (!param[i].Contains("="))
									parm.Value = null;
								else
								{
									//Extract setting string
									string v = param[i].Remove(0, param[i].IndexOf('=') + 1);

									//Switch based on various value types
									switch (parm.ValueType)
									{
										//For string parameters
										case Parameter.EValueType.String:
											parm.Value = v;
											//parm.CorrectVal = true;
											break;
										//For bool parameters
										case Parameter.EValueType.Bool:
											bool b = false;
											if (bool.TryParse(v, out b))
											{
												parm.Value = b;
												//parm.CorrectVal = true;
												break;
											}
											else
												parm.Value = null;
											//parm.CorrectVal = b;
											break;
										//For integer parameters
										case Parameter.EValueType.Int:
											int iv = 0;
											if (int.TryParse(v, out iv))
											{
												parm.Value = iv;
												//parm.CorrectVal = true;
												break;
											}
											else
												parm.Value = null;
											parm.Value = iv;
											break;
										//For float parameters
										case Parameter.EValueType.Float:
											float fv = 0.0f;
											if (float.TryParse(v, out fv))
											{
												parm.Value = fv;
												//parm.CorrectVal = true;
												break;
											}
											else
												parm.Value = null;
											parm.Value = fv;
											break;
										default:
											break;
									}
								}
								break;
							//For list parameters
							case Parameter.EType.List:
								//Initialize this parameter's value
								parm.Value = new List<string>();
								//Parse through the rest of the params until we find another param
								for(int b = i; b < param.Length; b++)
								{
									foreach(Parameter p in paramdesc)
									{
										//check if the parameter is contained in the list
										if (param[b].StartsWith(p.GetFullName()))
											goto End;
									}
									((List<string>)parm.Value).Add(param[b]);
									i++;
								}
								//If there are no elements we must have the wrong list
								if (((List<string>)parm.Value).Count <= 0)
									parm.Value = null;
								End:
								break;
						}
					}
				}
			}
		}
	}

	public class CommandParameterBase
	{
		public enum EType
		{
			Any,
			List,
			Int,
			Float,
			Bool,
			String,
			Date,
			Time,
			Double,
			Long,
		}

		public string Name { get; private set; }

		public EType Type { get; private set; }

		public bool Optional { get; private set; }

		public object Value { protected get; set; }

		public void Set(object val)
		{
			Value = val;
		}

		public bool IsValid()
		{
			return Value != null;
		}
	}

	public class CommandParameter<T> : CommandParameterBase
	{
		T Get()
		{
			return (T)Value;
		}
	}

	/// <summary>
	/// Rules:
	///		- If a parameter is a list parameter, it must be the last parameter in the list
	///		- If a parameter is optional, it must come after all parameters, cannot be used with lists
	///		- Parameters are parsed in order
	///		- Two optional parameters of the same type cannot be directly after one another
	///		- An optional parameter of the same type as the following parameter is not allowed
	/// </summary>
	public static class CommandParams
	{
		public static void Parse(ref List<CommandParameterBase> paramls, string[] param)
		{
			for(int i = 0; i < param.Length; i++)
			{
				foreach(CommandParameterBase b in paramls)
				{
					switch(b.Type)
					{
						case CommandParameterBase.EType.Bool:
							bool bo;
							if (bool.TryParse(param[i], out bo))
								b.Value = bo;
							else if (b.Optional)
								break;
							goto End;

						case CommandParameterBase.EType.Date:
							DateTime o;
							if (DateTime.TryParse(param[i], out o))
								b.Value = o;
							else if (b.Optional)
								break;
							goto End;

						case CommandParameterBase.EType.Int:
							int oi;
							if (int.TryParse(param[i], out oi))
								b.Value = oi;
							else if (b.Optional)
								break;
							goto End;

						case CommandParameterBase.EType.Float:
							float of;
							if (float.TryParse(param[i], out of))
								b.Value = of;
							else if (b.Optional)
								break;
							goto End;

						case CommandParameterBase.EType.List:
							List<string> ret = new List<string>();
							for (; i < param.Length; i++)
							{
								ret.Add(param[i]);
							}
							if (ret.Count > 0)
								b.Value = ret;
							goto End;

						default:
							b.Value = param[i];
							goto End;
					}
				}
				End:;
			}
		}
	}
}
