﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

/*
 * Defines a bunch of utilities for interpretation of languages
 */ 
namespace DiscordBotCS.Util.Communication
{
	internal struct LanguageDataTempHolder
	{
		public IList<WordEntry> Words { get; set; }
		public IDictionary<string, string> Contractions { get; set; }
	}

	public class LanguageData
	{
		public IDictionary<string, string> Contractions { get; set; }
		public IList<WordEntry> Prepositions { get; set; }
		public IList<WordEntry> Conjunctions { get; set; }
		public IList<WordEntry> Verbs { get; set; }
		public IList<WordEntry> Adverbs { get; set; }
		public IList<WordEntry> Pronouns { get; set; }
		public IList<WordEntry> Adjectives { get; set; }
		public IList<WordEntry> Determiners { get; set; }
		
		public string Language;

		public static LanguageData FromFile(String file)
		{
			using (StreamReader reader = new StreamReader(file))
			{
				JsonSerializer stream = new JsonSerializer();
				LanguageDataTempHolder ld = JsonConvert.DeserializeObject<LanguageDataTempHolder>(reader.ReadToEnd());
				LanguageData ret = new LanguageData();

				foreach(WordEntry entry in ld.Words)
				{
					if (entry.Type == (uint)EWordType.Adjective)
						ret.Adjectives.Add(entry);
				}

				return new LanguageData();
			}
		}
	}

	public enum EWordType
	{
		Adverb,
		Adjective,
		Noun,
		Conjunction,
		Verb,
		Pronoun,
		Preposition,
		Determiner,
		Exclamation,
	}

	public enum ESentenceType
	{
		Simple,
		Compound,
		Complex,
		Compound_Complex,
	}

	public enum ESentencePurpose
	{
		Declarative,
		Interrogative,
		Exclamatory,
		Imperative,
	}

	public class WordEntry
	{
		public uint Type { get; set; }
		public uint ID { get; set; }
		public string Word { get; set; }
		public string[] Aliases { get; set; }

		bool Equals(WordEntry word)
		{
			if (Type != word.Type)
				return false;
			if (ID != word.ID)
				return false;
			if (Word != word.Word)
				return false;
			foreach(string a in Aliases)
			{
				foreach(string f in word.Aliases)
				{
					if (f != a)
						return false;
				}
			}
			return true;
		}

		public static bool operator==(WordEntry ths, WordEntry other)
		{
			return ths.Equals(other);
		}

		public static bool operator!=(WordEntry ths, WordEntry other)
		{
			return ths.Equals(other);
		}

		public int CompareTo(string str)
		{
			int ret = 0;
			foreach(string alias in Aliases)
			{
				int val = 0;
				int len = str.Length <= alias.Length ? str.Length : alias.Length;

				for (int i = 0; i < len; i++)
				{
					if (str[i] == alias[i])
						val++;
				}
				ret = val >= ret ? val : ret;
			}
			return ret;
		}
	}

	public class WordData
	{
		/*
		 * Either Word or Data can be used based on what needs to be represented.
		 */
		public WordEntry Word;
		public string Data;

		bool Equals(WordData other)
		{
			if (Word == null)
				return other.Data == Data;
			else if (Data == null)
				return other.Word == Word;
			return true;
		}

		public static bool operator==(WordData ths, WordData other)
		{
			return ths.Equals(other);
		}

		public static bool operator!=(WordData ths, WordData other)
		{
			return ths.Equals(other);
		}
	}

	//Represents the clause in a sentence
	public class SentenceClause
	{
		/*
		 * Handles inversion of the actions in the predicate. We DO NOT want to have any negatives in there.
		 */ 
		public bool bInverted;

		/*
		 * The predicate, or what's happening to the subject
		 */ 
		public WordData[] Predicate;

		/*
		 * The subject of the clause, should be a string because certain things wont be defined.
		 * EX. Stacy's Mom isn't going to be in the dictionary
		 */ 
		public string Subject;

		public bool Equals(SentenceClause other)
		{
			foreach(WordData d in Predicate)
			{
				foreach(WordData d1 in other.Predicate)
				{
					if (d != d1)
						return false;
				}
			}
			if (bInverted != other.bInverted)
				return false;
			if (Subject != other.Subject)
				return false;
			return true;
		}

		public static bool operator==(SentenceClause cl1, SentenceClause cl2)
		{
			return cl1.Equals(cl2);
		}

		public static bool operator!=(SentenceClause cl1, SentenceClause cl2)
		{
			return cl1.Equals(cl2);
		}
	}

	public class Sentence
	{
		public ESentencePurpose Purpose;
		public ESentenceType Type;
		public SentenceClause[] Clauses;

		public bool Equals(Sentence o1)
		{
			if (o1.Purpose != Purpose)
				return false;
			if (o1.Type != Type)
				return false;
			foreach (SentenceClause s in o1.Clauses)
			{
				foreach (SentenceClause s1 in Clauses)
				{
					if (s != s1)
						return false;
				}
			}
			return true;
		}

		public static bool operator==(Sentence o1, Sentence o2)
		{
			return o1.Equals(o2);
		}

		public static bool operator!=(Sentence o1, Sentence o2)
		{
			return !o1.Equals(o2);
		}
	}
}
