﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Util.Communication
{
	public class SentenceBuilder
	{
		public LanguageData Lang { get; private set; }


		public SentenceBuilder(LanguageData lang)
		{
			Lang = lang;
		}

		public Sentence FromString(string Data)
		{
			Sentence ret = new Sentence();
			
			//Perform prune ops
			Data = Data.TrimEnd(' ');
			Data = Data.TrimStart(' ');

			//First we should determine the type of sentence
			ESentencePurpose purpose;
			if (Data.EndsWith("?"))
				purpose = ESentencePurpose.Interrogative;
			else if (Data.EndsWith("!"))
				purpose = ESentencePurpose.Exclamatory;
			else
				purpose = ESentencePurpose.Declarative;

			ret.Purpose = purpose;

			//Split our stupid data up!
			string[] sen = Data.Split(' ');



			return ret;
		}

		/*
		 * Returns the most similar word to the provided string.
		 * If word list is empty, none is returned.
		 */ 
		public WordEntry WordFromString(string Word)
		{
			if (Lang == null)
				throw new Exception("Language data is null!");
			if (Lang.Adjectives == null)
				throw new Exception("Word data is empty!");

			int bsim = 0;
			WordEntry bestMatch = null;
			foreach(WordEntry w in Lang.Adjectives)
			{
				int sim = w.CompareTo(Word);
				if(sim > bsim)
				{
					bsim = sim;
					bestMatch = w;
				}
			}
			return bestMatch;
		}

	}
}
