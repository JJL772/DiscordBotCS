﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Util
{
	/*
	 * Entry in unit conversion table.
	 * FromAlias = Alias you're converting from
	 * ToAlias = Alias you're converting to
	 * ConversionConstant = The constant of conversion. The number of ToAlias in FromAlias
	 */ 
	public class UnitConversionEntry
	{
		List<string> FromAlias = new List<string>();
		List<string> ToAlias = new List<string>();
		Func<bool, float, float> ConversionFunction = null;
		float ConversionConstant;

		public UnitConversionEntry(string from, string to, float constant)
		{
			FromAlias.Add(from);
			ToAlias.Add(to);
			ConversionConstant = constant;
		}

		public UnitConversionEntry(string[] from, string[] to, float constant)
		{
			foreach (string s in from)
				FromAlias.Add(s);
			foreach (string s in to)
				ToAlias.Add(s);
			ConversionConstant = constant;
		}

		/*
		 * BECAUSE SOMETIMeS YOU NEED FUnCtIonS
		 */ 
		public UnitConversionEntry(string[] from, string[] to, Func<bool, float, float> convFn)
		{
			ConversionFunction = convFn;
			foreach (string s in from)
				FromAlias.Add(s);
			foreach (string s in to)
				ToAlias.Add(s);
		}

		public float GetConversionConstant()
		{
			return ConversionConstant;
		}

		public void AddFromAlias(string alias)
		{
			FromAlias.Add(alias);
		}

		public void AddToAlias(string alias)
		{
			ToAlias.Add(alias);
		}

		public void RemoveFromAlias(string alias)
		{
			FromAlias.Remove(alias);
		}

		public void RemoveToAlias(string alias)
		{
			ToAlias.Remove(alias);
		}

		public float PerformConversion(string from, string to, float ammt)
		{
			//If we're converting from our fromAlias, then multiply
			foreach(string alias in FromAlias)
			{
				if (alias == from && ConversionFunction == null)
					return ammt * ConversionConstant;
				else if (alias == from && ConversionFunction != null)
					return ConversionFunction(false, ammt);
			}
			//Else, lets perform the other operation
			if (ConversionFunction == null)
				return ammt / ConversionConstant;
			else
				return ConversionFunction(true, ammt);
		}

		public bool ContainsOperation(string from, string to)
		{
			bool b1 = false;
			bool b2 = false;
			foreach(string alias in FromAlias)
			{
				if (alias == from)
				{
					b1 = true;
					break;
				}
				else if(alias == to)
				{
					b2 = true;
					break;
				}
			}
			foreach(string alias in ToAlias)
			{
				if (alias == to)	
				{
					b2 = true;
					break;
				}
				else if(alias == from && b1 != true)
				{
					b1 = true;
					break;
				}
			}
			return b1 && b2;
		}

		public void AddAliases(String[] to, String[] from)
		{
			foreach (string s in to)
				ToAlias.Add(s);
			foreach (string s in from)
				FromAlias.Add(s);
		}
	}
	public class UnitConverter
	{
		private List<UnitConversionEntry> UnitConversions = new List<UnitConversionEntry>();

		public UnitConverter()
		{
			
		}

		public void AddConversionEntry(UnitConversionEntry entry)
		{
			UnitConversions.Add(entry);
		}

		public void RemoveConversionEntry(UnitConversionEntry entry)
		{
			UnitConversions.Remove(entry);
		}

		public bool ContainsEntry(string from, string to)
		{
			foreach(UnitConversionEntry e in UnitConversions)
			{
				if (e.ContainsOperation(from, to))
					return true;
			}
			return false;
		}

		public float ConvertUnits(string from, string to, float ammt)
		{
			foreach(UnitConversionEntry e in UnitConversions)
			{
				if(e.ContainsOperation(from, to))
					return e.PerformConversion(from, to, ammt);
			}
			return 0.0f;
		}
	}
}
