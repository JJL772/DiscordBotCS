﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using DSharpPlus;
using DSharpPlus.Entities;
using DiscordBotCS.Bot;

namespace DiscordBotCS.Util
{
	internal static class DiscordUtils
	{
		//This is a reference token for discord, this isn't an actual token, it has a few characters changed in it
		private static String ReferenceToken = "MzU5NDcxMjU0MDE0Nzg3NTk2.KKhyAw.X10KmFArdIHMtUMdngNrkJqvhB0";

		//Checks if the user is an admin
		public static bool IsUserAdmin(DiscordMember user)
		{
			//Null checkin
			if(user == null) { return false; };

			DiscordRole[] Roles = user.Roles.ToArray();
			
			for(int i = 0; i < Roles.Length; i++)
			{
				if (Roles[i].CheckPermission(Permissions.Administrator) == PermissionLevel.Allowed)
					return true;
			}
			return false;
		}

		public static DiscordMember DiscordUserToMember(DiscordUser user, DiscordGuild guild)
		{
			foreach(DiscordMember mem in guild.Members)
			{
				if(user.Id == mem.Id)
				{
					return mem;
				}
			}
			return null;
		}

		//Checks if the user has the specified permission
		public static bool HasPermission(DiscordMember user, Permissions permission)
		{
			DiscordRole[] Roles = user.Roles.ToArray();

			for(int i = 0; i < Roles.Length; i++)
			{
				if (Roles[i].CheckPermission(permission) == PermissionLevel.Allowed)
					return true;
			}
			return false;
		}

		//Checks if the user has the specified role
		public static bool HasRole(DiscordMember user, DiscordRole role)
		{
			DiscordRole[] Roles = user.Roles.ToArray();

			for(int i = 0; i < Roles.Length; i++)
			{
				if (Roles[i] == role)
					return true;
			}
			return false;
		}

		//For security reasons, mainly so we can push to github w/o censored code
		public static bool ReadTokenFromFile(String file, out String token)
		{
			try
			{
				if(File.Exists(file))
				{
					String Buffer = File.ReadAllText(file);
					if(true)
					{
						token = Buffer;
						return true;
					}
					else
					{
						token = null;
						return false;
					}
				}
				else
				{
					token = null;
					return false;
				}
			}
			catch(Exception e)
			{
				token = null;
				return false;
			}
		}

		public static bool ValidateArguments(uint argCount, string[] args)
		{
			return args.Length >= argCount;
		}

		public static void PrintArgumentError(string inputString, uint mistakePos, string expectedFormat, DiscordMessage msg)
		{
			string response = "```\nSyntax error at: \n" + inputString + "\n";
			for(uint i = 0; i < inputString.Length && i != mistakePos - 1; i++)
			{
				response += " ";
			}
			response += "^\n";
			response += "Expected argument, got null \nNote: use syntax: " + expectedFormat + "\n```";
			msg.RespondAsync(response);
		}
	}
}
