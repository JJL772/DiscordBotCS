﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//First of all I'm really lazy, so most things are very rigid
namespace DiscordBotCS.Util
{
	public struct PCMData8Bit
	{
		public byte[] PCMData;
		public int BitRate;
	}

	public struct PCM16BitSigned
	{
		public short[] PCMData;
		public int BitRate;
	}

	public struct PCM16BitUnsigned
	{
		public UInt16[] PCMData;
		public int BitRate;
	}

	public struct PCM32BitFloat
	{
		public float[] PCMData;
		public int BitRate;
	}

	public enum EMP3Version
	{
		MPEG_2_5 = 0b00,
		Reserved = 0b01,
		MPEG_2 = 0b10,
		MPEG_1 = 0b11,
	}

	public enum EMP3Layer
	{
		Reserved = 0b00,
		LayerI = 0b01,
		LayerII = 0b10,
		LayerIII = 0b11,
	}

	public enum EMP3ChannelMode
	{
		Stereo = 0b00,
		JointStereo = 0b01,
		DualChannel = 0b10,
		SingleChannel = 0b11,
	}

	public enum EMP3Frequency_MPEG1
	{
		Freq_44100 = 0b00,
		Freq_48000 = 0b01,
		Freq_32000 = 0b10,
		Reserved = 0b11,
	}

	public enum EMP3Frequency_MPEG2
	{
		Freq_22050 = 0b00,
		Freq_24000 = 0b01,
		Freq_16000 = 0b10,
		Reserved = 0b11,
	}

	public enum EMP3Frequency_MPEG2_5
	{
		Freq_11025 = 0b00,
		Freq_12000 = 0b01,
		Freq_8000 = 0b10,
		Reserved = 0b11,
	}

	public enum EMP3BitRate_Layer3_MPEG2
	{
		Rate_Free = 0b00,
		Rate_8 = 0b0001,
		Rate_16 = 0b0010,
		Rate_24 = 0b0011,
		Rate_32 = 0b0100,
		Rate_40 = 0b0101,
		Rate_48 = 0b0110,
		Rate_56 = 0b0111,
		Rate_64 = 0b1000,
		Rate_80 = 0b1001,
		Rate_90 = 0b1010,
		Rate_112 = 0b1011,
		Rate_128 = 0b1100,
		Rate_144 = 0b1101,
		Rate_160 = 0b1110,
		Rate_BAD = 0b1111,
	}

	public enum EMP3BitRate_Layer3_MPEG1
	{
		Rate_Free = 0b0000,
		Rate_32 = 0b0001,
		Rate_40 = 0b0010,
		Rate_48 = 0b0011,
		Rate_56 = 0b0100,
		Rate_64 = 0b0101,
		Rate_80 = 0b0110,
		Rate_96 = 0b0111,
		Rate_112 = 0b1000,
		Rate_128 = 0b1001,
		Rate_160 = 0b1010,
		Rate_192 = 0b1011,
		Rate_224 = 0b1100,
		Rate_256 = 0b1101,
		Rate_320 = 0b1110,
		Rate_BAD = 0b1111,
	}

	public enum EMP3Masks : uint
	{
		FrameSync_Mask	 =		0b1111_1111_1110_0000_0000_0000_0000_0000,
		Version_Mask	 =		0b0000_0000_0001_1000_0000_0000_0000_0000,
		LayerDesc_Mask	 =		0b0000_0000_0000_0110_0000_0000_0000_0000,
		Protection_Mask	 =		0b0000_0000_0000_0001_0000_0000_0000_0000,
		Bitrate_Mask	 =		0b0000_0000_0000_0000_1111_0000_0000_0000,
		Freq_Mask		 =		0b0000_0000_0000_0000_0000_1100_0000_0000,
		Padding_Mask	 =		0b0000_0000_0000_0000_0000_0010_0000_0000,
		Private_Mask	 =		0b0000_0000_0000_0000_0000_0001_0000_0000,
		Channel_Mask	 =		0b0000_0000_0000_0000_0000_0000_1100_0000,
		ModeExt_Mask	 =		0b0000_0000_0000_0000_0000_0000_0011_0000,
		Copyright_Mask	 =		0b0000_0000_0000_0000_0000_0000_0000_1000,
		Original_Mask	 =		0b0000_0000_0000_0000_0000_0000_0000_0100,
		Emphasis_Mask	 =		0b0000_0000_0000_0000_0000_0000_0000_0011,
	}

	public struct MP3FrameHeader_MPEG2
	{
		short FrameSync;
		EMP3Version Version;
		EMP3Layer LayerDesc;
		bool CRCProtected;
		EMP3BitRate_Layer3_MPEG2 BitRate;
		bool Padded;
		EMP3ChannelMode ChannelMode;
		bool Copyright;
		bool Original;
		char Emphasis;

		public static MP3FrameHeader_MPEG2 FromInt(int data)
		{
			MP3FrameHeader_MPEG2 ret = new MP3FrameHeader_MPEG2();
			ret.FrameSync = (short)(data & (uint)EMP3Masks.FrameSync_Mask);
			ret.Version = (EMP3Version)(data & (uint)EMP3Masks.Version_Mask);
			ret.LayerDesc = (EMP3Layer)(data & (uint)EMP3Masks.LayerDesc_Mask);
			ret.CRCProtected = Boolean.Parse((data & (uint)EMP3Masks.Protection_Mask).ToString());
			ret.BitRate = (EMP3BitRate_Layer3_MPEG2)(data & (uint)EMP3Masks.Bitrate_Mask);
			ret.Padded = Boolean.Parse((data & (uint)EMP3Masks.Padding_Mask).ToString());
			ret.ChannelMode = (EMP3ChannelMode)(data & (uint)EMP3Masks.Channel_Mask);
			ret.Copyright = Boolean.Parse((data & (uint)EMP3Masks.Copyright_Mask).ToString());
			ret.Original = Boolean.Parse((data & (uint)EMP3Masks.Original_Mask).ToString());
			ret.Emphasis = (char)(data & (uint)EMP3Masks.Emphasis_Mask);
			return ret;
		}
	}

	public struct MP3SoundFrame_MPEG2
	{

	}

	public struct WAV16BitSigned
	{
		public short[] AudioData;
		public char ChunkSize;
		public char ChunkID;

	}

	public class SoundConverters
	{

	}
}
