﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace DiscordBotCS.Util
{
	public class Logger
	{
		public bool LogToFile { get; private set; }

		public string OutFile { get; private set; }

		//Flare, ie. [myflare] Logged info!
		public string Flare { get; private set; }
		
		private StreamWriter FileStream;

		public Logger(string file, string flare, bool bToFile = true)
		{
			Flare = flare;

			LogToFile = bToFile;

			if (bToFile)
			{
				//Lazy so do this
				try
				{
					FileStream = new StreamWriter(file);
					OutFile = file;
				}
				catch (Exception e)
				{
					throw e;
				}
			}
		}

		~Logger()
		{
			if (LogToFile)
			{
				FileStream.Flush();
				FileStream.Close();
			}
		}

		public void Log(params object[] list)
		{
			Console.WriteLine(Flare, " ", list);

			//Log to file
			if (LogToFile)
			{
				FileStream.WriteLine(Flare, " ", list);
				FileStream.FlushAsync();
			}
		}

		public void LogWarning(params object[] list)
		{
			ConsoleColor bck = Console.BackgroundColor;
			ConsoleColor fore = Console.ForegroundColor;

			Console.BackgroundColor = ConsoleColor.Yellow;
			Console.ForegroundColor = ConsoleColor.Black;

			Console.WriteLine(Flare, "[WARN] ", list);

			//Log to file
			if (LogToFile)
			{
				FileStream.WriteLine(Flare, "[WARN] ", list);
				FileStream.FlushAsync();
			}

			Console.BackgroundColor = bck;
			Console.ForegroundColor = fore;
		}

		public void LogError(params object[] list)
		{
			ConsoleColor bck = Console.BackgroundColor;
			ConsoleColor fore = Console.ForegroundColor;

			Console.BackgroundColor = ConsoleColor.Red;
			Console.ForegroundColor = ConsoleColor.White;

			Console.WriteLine(Flare, "[ERR] ", list);

			//Log to file
			if (LogToFile)
			{
				FileStream.WriteLine(Flare, "[ERR] ", list);
				FileStream.FlushAsync();
			}

			Console.BackgroundColor = bck;
			Console.ForegroundColor = fore;
		}

		public void LogSuccess(params object[] list)
		{
			ConsoleColor bck = Console.BackgroundColor;
			ConsoleColor fore = Console.ForegroundColor;

			Console.BackgroundColor = ConsoleColor.Green;
			Console.ForegroundColor = ConsoleColor.Black;

			Console.WriteLine(Flare, list);

			//Log to file
			if (LogToFile)
			{
				FileStream.WriteLine(Flare, " ", list);
				FileStream.FlushAsync();
			}

			Console.BackgroundColor = bck;
			Console.ForegroundColor = fore;
		}

		public void LogFatal(params object[] list)
		{
			ConsoleColor bck = Console.BackgroundColor;
			ConsoleColor fore = Console.ForegroundColor;

			Console.BackgroundColor = ConsoleColor.Red;
			Console.ForegroundColor = ConsoleColor.White;

			Console.WriteLine(Flare, "[FATAL] ", list);

			//Log to file
			if (LogToFile)
			{
				FileStream.WriteLine(Flare, "[FATAL] ", list);
				FileStream.FlushAsync();
			}

			Console.BackgroundColor = bck;
			Console.ForegroundColor = fore;
		}
	}
}
