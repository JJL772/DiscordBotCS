﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Util
{
	public static class StringUtil
	{
		/*
		 * Similar to C's strcmp function, except we return the number of similar characters, allowing for smart prediction
		 * PERFORMANCE WARNING: Loop iterations = String1.Length * String2.Length
		 */ 
		public static int StrCmp(string str1, string str2)
		{
			int similarity = 0;
			foreach(char c1 in str1)
			{
				foreach(char c2 in str2)
				{
					if (c1 == c2)
						similarity += 1;
				}
			}
			return similarity;
		}

		/*
		 * Similar to C's strcmp function, except we return the percent of similar characters, allowing for smart prediction
		 * PERFORMANCE WARNING: Loop iterations = String1.Length * String2.Length
		 */
		public static float StringSimilarity(string str1, string str2)
		{
			int simchar = 0;
			foreach(char c1 in str1)
			{
				foreach(char c2 in str2)
				{
					if (c1 == c2)
						simchar += 1;
				}
			}
			//Forgive me, for I have sinned.
			float longt = str1.Length >= str2.Length ? (float)str1.Length : (float)str2.Length;
			return simchar / longt * 100.0f;
		}

		public static bool IsFloat(String s)
		{
			float f;
			return float.TryParse(s, out f);
		}

		public static bool IsDouble(String s)
		{
			double d;
			return double.TryParse(s, out d);
		}

		public static bool IsBool(String s)
		{
			bool b;
			return bool.TryParse(s, out b);
		}

		public static bool IsInt(String s)
		{
			int i;
			return int.TryParse(s, out i);
		}

		public static bool IsLong(String s)
		{
			long i;
			return long.TryParse(s, out i);
		}

		public static bool IsDate(String s)
		{
			DateTime res;
			return DateTime.TryParse(s, out res);
		}
	}
}
