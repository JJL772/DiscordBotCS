﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DiscordBotCS.Util
{
	/*
	 * ThreadPool
	 * This is a pool of threads!
	 * 
	 */ 
	public class ThreadPool
	{
		private UInt16 MaxThreads;
		private List<Thread> Threads;

		public ThreadPool(UInt16 threads)
		{
			MaxThreads = threads;
		}

		public void QueryAction(Action action)
		{
			if(Threads.Count < MaxThreads)
			{
				Thread thr = new Thread(new ThreadStart(action));
				Threads.Add(thr);
			}
		}
	}
}
