﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DiscordBotCS.Util.ImageProcessing
{
	public struct HTMLImage
	{
		public string Data;
	}

	public static class BitmapToHTML
	{
		public static HTMLImage? ConvertBitmapToHTML(Bitmap img)
		{
			HTMLImage retImg = new HTMLImage();
			retImg.Data = "<!DOCTYPE html><html><body><style>p{display:inline;font-family:\"Consolas\",Times,Serif;}body{background-color:black;}</style><div class=\"p\">";

			//Resize image
			int width = 125;
			int height = (int)Math.Round(img.Height * (75.0f / img.Width));
			Bitmap f = new Bitmap(img, width, height);
			img.Dispose();
			img = f;

			if (img == null)
				return null;

			if (img.PixelFormat == PixelFormat.Canonical || img.PixelFormat == PixelFormat.DontCare || img.PixelFormat == PixelFormat.Extended || img.PixelFormat == PixelFormat.Undefined ||
				img.PixelFormat == PixelFormat.Indexed || img.PixelFormat == PixelFormat.Max || img.PixelFormat == PixelFormat.Gdi)
				return null;

			for(int y = 0; y < img.Height; y++)
			{
				for(int x = 0; x < img.Width; x++)
				{
					Color c = img.GetPixel(x, y);
					retImg.Data += "<p style=\"color:rgb(" + c.R + "," + c.G + "," + c.B + ");\">0</p>\n";
					if (x == img.Width - 1)
						retImg.Data += "<br>";
				}
			}
			retImg.Data += "</div></body></html>";
			img.Dispose();
			return retImg;
		}
	}
}
