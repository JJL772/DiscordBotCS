﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace DiscordBotCS.Util.ImageProcessing
{
	public static class BitmapToASCII
	{
			public struct ASCIIImage
			{
				public uint width;
				public uint height;
				public char[,] ImageContents;
			}

			public static ASCIIImage? ConvertBitmapToASCII(Bitmap img)
			{
				if (img == null)
					return null;

				if (img.PixelFormat == PixelFormat.Undefined)
				{
					return null;
				}
				else if (img.PixelFormat == PixelFormat.Indexed || img.PixelFormat == PixelFormat.Gdi)
				{
					return null;
				}

				ASCIIImage returnedImg = new ASCIIImage();

				if (img.Width > 100 && img.Width > 0)
				{
					float aspectRatio = img.Width / img.Height;
					int width = 100;
					float height = (img.Height * (100.0f / img.Width));
					Bitmap f = new Bitmap(img, width, (int)height);
					img.Dispose();
					img = f;
				}

				Bitmap gd = new Bitmap(img, (int)(img.Width * 2), (int)(img.Height));
				img.Dispose();
				img = gd;

				returnedImg.ImageContents = new char[img.Width, img.Height];
				returnedImg.width = (uint)img.Width;
				returnedImg.height = (uint)img.Height;

				//Convert to grayscale
				for (int x = 0; x < img.Width; x++)
				{
					for (int y = 0; y < img.Height; y++)
					{
						Color c = img.GetPixel(x, y);
						byte avg = (byte)((c.R + c.G + c.B) / 3);
						c = Color.FromArgb(avg, avg, avg);
						img.SetPixel(x, y, c);
					}
				}

				for (int y = 0; y < img.Height; y++)
				{
					for (int x = 0; x < img.Width; x++)
					{
						//alreadt grayscale, so we can assume r == g == b
						byte clr = img.GetPixel(x, y).R;

						if (clr <= 20)
							returnedImg.ImageContents[x, y] = ' ';
						else if (clr <= 40)
							returnedImg.ImageContents[x, y] = '`';
						else if (clr <= 60)
							returnedImg.ImageContents[x, y] = '.';
						else if (clr <= 80)
							returnedImg.ImageContents[x, y] = '*';
						else if (clr <= 100)
							returnedImg.ImageContents[x, y] = '!';
						else if (clr <= 120)
							returnedImg.ImageContents[x, y] = 'i';
						else if (clr <= 140)
							returnedImg.ImageContents[x, y] = 'j';
						else if (clr <= 160)
							returnedImg.ImageContents[x, y] = 'l';
						else if (clr <= 180)
							returnedImg.ImageContents[x, y] = '0';
						else if (clr <= 200)
							returnedImg.ImageContents[x, y] = 'F';
						else if (clr <= 220)
							returnedImg.ImageContents[x, y] = 'R';
						else if (clr <= 240)
							returnedImg.ImageContents[x, y] = 'B';
						else if (clr <= 255)
							returnedImg.ImageContents[x, y] = '#';
						else
							returnedImg.ImageContents[x, y] = '@';
					}
				}

				return returnedImg;
			}
	}
}
