﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace DiscordBotCS.Util.Data
{
	/*
	Basic config info
	*/
	public class Config
	{
		public static Config FromFile(string file)
		{
			try
			{
				string json = System.IO.File.ReadAllText(file);
				Config cjson = JsonConvert.DeserializeObject<Config>(json);
				return cjson;
			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public string GmailAPISecret { get; private set; }

		public string GmailAPIClientID { get; private set; }

		public string DiscordClientSecret { get; private set; }

		public string SteamAPIKey { get; private set; }

		public string ImgurAPIKey { get; private set; }

		public string SQLIP { get; private set; }

		public string SQLPort { get; private set; }

		public string SQLPassword { get; private set; }

		public string SQLConfigFile { get; private set; }

		public string CommandListFile { get; private set; }

		public string LocaleFile { get; private set; }
	}

	/*
	Table names, etc.
	*/
	public class SQLConfig
	{

		public SQLConfig FromFile(string file)
		{
			try
			{
				string json = File.ReadAllText(file);
				SQLConfig cjson = JsonConvert.DeserializeObject<SQLConfig>(json);
				return cjson;
			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public string UsersTable { get; private set; }

		public string AdminsTable { get; private set; }

		public string GuildsTable { get; private set; }
	}

	public class LocaleConfig
	{

		public LocaleConfig FromFile(string file)
		{
			try
			{
				string json = File.ReadAllText(file);
				LocaleConfig cjson = JsonConvert.DeserializeObject<LocaleConfig>(json);
				return cjson;
			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public string EnglishLocale { get; private set; }

		public string GermanLocale { get; private set; }

		public string SpanishLocale { get; private set; }

		public string FrenchLocale { get; private set; }
	}
}
