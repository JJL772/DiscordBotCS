﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Util.Data.Protobuf;
using System.IO;
using Google.Protobuf;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;

namespace DiscordBotCS.Util.Data
{
	public static class UserUtil
	{
		public static User GetUserByID(ulong id, DiscordGuild guild, DiscordBot bot)
		{
			foreach(DiscordMember m in guild.Members)
			{
				foreach(User u in bot.UserList.Users)
				{
					if (m.Id == u.ID)
						return u;
				}
			}
			return null;
		}

		public static User GetUserByUsername(string username, DiscordGuild guild, DiscordBot bot)
		{
			foreach (DiscordMember m in guild.Members)
			{
				foreach (User u in bot.UserList.Users)
				{
					if (m.Username == u.Username)
						return u;
				}
			}
			return null;
		}

		public static User GetUserByMention(string mentionString, DiscordGuild guild, DiscordBot bot)
		{
			foreach(DiscordMember m in guild.Members)
			{
				if(m.Mention == mentionString)
				{
					foreach(User u in bot.UserList.Users)
					{
						if (u.ID == m.Id)
							return u;
					}
				}
			}
			return null;
		}
	}

	public static class UserLoader
	{
		public static UserList LoadFromFile()
		{
			if (!File.Exists("Data/UserData.dat"))
			{
				return UserList.Parser.ParseFrom(File.Create("Data/UserData.dat"));
			}

			byte[] FileData = File.ReadAllBytes("Data/UserData.dat");
			UserList ul = UserList.Parser.ParseFrom(FileData);

			return ul;
		}

		public static void SaveToFile(UserList list)
		{
			if(list != null)
			{
				CodedOutputStream fs = new CodedOutputStream(File.Create("Data/UserData.dat"));

				list.WriteTo(fs);
				fs.Flush();
				fs.Dispose();
			}
		}

		public static void UpdateUsers(DiscordBot bot)
		{
			bot.UserList = null;
			bot.UserList = LoadFromFile();

			foreach(KeyValuePair<ulong, DiscordGuild> pair in bot.Client.Guilds)
			{
				DiscordGuild d = pair.Value;
				foreach(DiscordMember m in d.Members)
				{
					bool Skip = false;
					foreach(User u in bot.UserList.Users)
					{
						if (m.Id == u.ID)
							Skip = true;
					}

					if(!Skip)
					{
						User u = new User();
						u.Nickname = m.Nickname == null ? m.Username : m.Nickname;
						u.Username = m.Username == null ? "UNDEFINED" : m.Username;
						u.Points = 0;
						u.Level = 1;
						u.ID = m.Id;
						u.LastDownvoteTime = 0;
						u.Money = 0.0f;
						if (m.IsBot)
							u.Type = User.Types.TypeOfUser.Bot;
						else
							u.Type = User.Types.TypeOfUser.User;
						bot.UserList.Users.Add(u);
					}
				}
			}
			SaveToFile(bot.UserList);
		}
	}
}
