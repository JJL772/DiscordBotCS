﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace DiscordBotCS.Util.Data
{
	/*
	Command listing JSON file
	*/
	public class JSONCommandList
	{
		/*
		Parse from file
		*/
		public static JSONCommandList FromFile(string file)
		{
			try
			{
				string json = File.ReadAllText(file);
				JSONCommandList list = JsonConvert.DeserializeObject<JSONCommandList>(json);
				return list;
			}
			catch(Exception e)
			{
				throw e;
			}
		}

		/*
		List of commands
		*/
		[JsonProperty("Commands")]
		public IList<CommandDescriptor> CommandList { get; private set; }
	}
	
	/*
	Command data in the JSON list
	*/
	public class CommandDescriptor
	{
		/*
		Name of the command, will serve as a first alias
		This name must match up with the handler class' attribute name

		Example:

		--Commands.json--
		Command.Name = "Test";

		--TestCommand.cs--
		[Command("Test")]
		class TestCommand
		{ ... }
		*/
		public string Name { get; private set; }

		/*
		Aliases for the command, ie: {list, ls, lis}
		*/
		public IList<string> Aliases { get; private set; }

		/*
		Short version of the helptext, printed in the help menu
		*/
		public string ShortHelp { get; private set; }

		/*
		Long version of the helptext, printed in the more specific help menu
		$help mycommand
		*/
		public string LongHelp { get; private set; }

		/*
		Developer commands can only be used by developers
		Developers are specified in the SQL database
		*/
		public bool DevCommand { get; private set; }

		/*
		If this command is hidden from the help menu
		*/
		public bool Hidden { get; private set; }

		/*
		Permissions test
		Will be implemented later...probably
		List of roles a user must have to use the command
		*/
		public IList<string> RequiredRoles { get; private set; }

		/*
		Parameter list, if empty no parameters can be supplied
		*/
		public IList<JSONCommandParam> Params { get; private set; }

		/*
		When true, parameters will be enforced. When the command is called the params list will be checked.
		*/
		public bool EnforceParams { get; private set; }

		/*
		The corresponding command class in which to use
		*/
		[JsonIgnore]
		public Type CommandClass { get; set; }

		public delegate void RunFn(IList<string> parameters, string fulllist, CommandDescriptor desc);

		[JsonIgnore]
		public RunFn Run { get; set; }
	}

	public class JSONCommandParam
	{

	}
}
