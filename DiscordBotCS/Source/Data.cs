﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Commands;
using DSharpPlus.Entities;
using DiscordBotCS.Commands.Steam;
using DiscordBotCS.Commands.Math;
using DiscordBotCS.Commands.Triggers;
using DiscordBotCS.Commands.Imgur;
using DiscordBotCS.Commands.Image;
using DiscordBotCS.Commands.Debug;
using DiscordBotCS.Commands.User;

namespace DiscordBotCS
{
	public static class Data
	{


		public static Dictionary<string, string> InfoCommandFields = new Dictionary<string, string>()
		{
			{"Version:", "v0.1.2" },
			{"Author:", "JJl77" },
			{"Other Stuff:", "Please end me I have no life" },
		};

		public static String[] NukeImgs =
		{
			"https://media.tenor.com/images/b74dd70e837d45465cfabb6bd2b44f99/tenor.gif",
			"https://vignette.wikia.nocookie.net/epicrapbattlesofhistory/images/b/b2/Russian_nuke.gif",
			"https://m.popkey.co/0fe631/M50kL.gif",
			"http://i0.kym-cdn.com/photos/images/original/000/973/192/638.gif",
			"https://media.giphy.com/media/Q9hST6BX40lY4/giphy.gif",
			"http://i.imgur.com/bs435lo.gif",
			"http://i.imgur.com/Fb40nx1.gif",
			"http://chan.catiewayne.com/c/src/138998789759.gif",
			"http://vignette2.wikia.nocookie.net/knightsanddragons/images/d/d5/Nuke-GIF.gif/revision/latest?cb=20140512020715",
			"https://i.imgur.com/VXgFUJu.gif",
			"https://i.imgur.com/OFNE2Lc.gif",
			"https://media.giphy.com/media/ETIbfzNt885zi/giphy.gif",
			"https://media.giphy.com/media/oVodBvZgtrB72/giphy.gif",
			"https://media.giphy.com/media/G78xhDnsTTRvO/giphy.gif",
		};

		public static String[] EightBallResponses =
		{
			"It is certain",
			"It is decidedly so",
			"Without a doubt",
			"Yes definitely",
			"You may rely on it",
			"As I see it, yes",
			"Most likely",
			"Outlook good",
			"Yes",
			"Signs point to yes",
		};

		public static string[] FAliases =
		{
			"fahrenheit",
			"f",
		};

		public static string[] CelsiusAliases =
		{
			"celsius",
			"c",
		};

		public static string[] KelvinAliases =
		{
			"kelvin",
			"k",
		};

		//Aliases and stuff
		public static string[] MeterAliases =
		{
			"meter",
			"meters",
			"m"
		};

		public static string[] MileAliases =
		{
			"mile",
			"miles",
			"mi",
		};

		public static string[] InchAliases =
		{
			"inch",
			"inches",
			"in",
		};

		public static string[] FeetAliases =
		{
			"foot",
			"ft",
			"feet",
		};

		public static string[] KilometerAliases =
		{
			"kilometer",
			"kilometers",
			"km",
		};

		public static string[] CentimeterAliases =
		{
			"cm",
			"centimeters",
			"centimeter",
		};

		public static string[] YardAliases =
		{
			"yd",
			"yard",
			"yards",
		};

		public static string[] LiterAliases =
		{
			"l",
			"liter",
			"liters",
		};

		public static string[] GallonAliases =
		{
			"gal",
			"gallon",
			"gallons",
		};

		public static string[] QuartAliases =
		{
			"qt",
			"quart",
			"quarts",
		};

		public static string[] PintAliases =
		{
			"pt",
			"pint",
			"pints",
		};

		public static string[] MilliliterAliases =
		{
			"ml",
			"milliliter",
			"milliliters",
		};

		public static string[] JoulesAliases =
		{
			"j",
			"joule",
			"joules",
		};

		public static string[] BTUAliases =
		{
			"btu",
			"britishthermalunits",
			"fuckthisunit",
		};

		public static string[] NauticalMileAliases =
		{
			"nmi",
			"nauticalmile",
			"nauticalmiles",
		};

		public static string[] CupAliases =
		{
			"cup",
			"cups",
		};

		public static string[] TeaSpoonAliases =
		{
			"tsp",
			"teaspoons",
			"teaspoon",
		};

		public static string[] TableSpoonAliases =
		{
			"tbsp",
			"tablespoon",
			"tablespoons",
		};

		public static string[] AtmAliases =
		{
			"atm",
			"atmosphere",
			"atmospheres",
		};

		public static string[] PSIAliases =
		{
			"psi",
			"lbsqin",
			"lbinsq",
			"lbin^2",
		};

		public static string[] mmHGAliases =
		{
			"torr",
			"mmhg",
		};

		public static string[] BarAliases =
		{
			"bar",
		};

		public static string[] PascalsAliases =
		{
			"pascal",
			"pascals",
		};

		public static string[] OunceAliases =
		{
			"oz",
			"ounce",
			"ounces",
		};

		public static string[] PoundAliases =
		{
			"lb",
			"lbs",
			"pound",
			"pounds",
		};

		public static string[] KilogramAliases =
		{
			"kg",
			"kilos",
			"kilogram",
			"kilograms",
		};

		public static string[] GramAliases =
		{
			"gram",
			"grams",
			"g",
		};

		public static string[] NewtonAliases =
		{
			"n",
			"newton",
			"newtons",
		};

		public static string[] SquareFootAliases =
		{
			"sqft",
			"squarefeet",
			"ft^2",
		};

		public static string[] SquareMilesAliases =
		{
			"sqmi",
			"mi^2",
			"squaremiles",
		};

		public static string[] SquareYardsAliases =
		{
			"sqyd",
			"yd^2",
			"squareyards",
		};

		public static string[] AcreAliases =
		{
			"acre",
		};

		public static string[] SquareKilometersAliases =
		{
			"sqkm",
			"km^2",
			"squarekilometers",
		};

		public static string[] CalorieAliases =
		{
			"cal",
			"calorie",
			"calories",
		};

		public static string[] KCalAliases =
		{
			"kcal",
			"kilocalorie",
			"kilocaliries",
			"kcalories",
			"kcalorie",
		};

		public static string[] DegreeAliases =
		{
			"deg",
			"degree",
			"degrees",
		};

		public static string[] RadianAliases =
		{
			"rad",
			"radians",
			"radian",
		};

		public static BaseCommand[] Commands =
		{
			new HelpCommand(),
			new InfoCommand(),
			new NukeCommand(),
			new PokeCommand(),
			new RemoveMessagesCommand(),
			new SteamProfileCommand(),
			new SineWaveCommand(),
			new RegisterChatTriggerCommand(),
			new MemeCommand(),
			new SimpleExpressionSolver(),
			new BitmapToASCII(),
			new DebugSaveData(),
			new DebugLoadData(),
			new DebugUpdateData(),
			new DebugPrintUsers(),
			new UpvoteUserCommand(),
			new DownvoteUserCommand(),
			new EvaluateCommand(),
			new UnitConverterCommand(),
			new BitmapToHTMLCommand(),
			new EquationRenderer(),
		};
	}
}
