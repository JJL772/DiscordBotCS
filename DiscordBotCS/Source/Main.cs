﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DiscordBotCS.Util;
using System.IO;
using DiscordBotCS.Util.Data;

namespace DiscordBotCS
{
    public class Application
    {
		public static Logger Log = new Logger("Logs/log.txt", "[DiscordBotCS]", true);

		public static Logger DBLog = new Logger("Logs/dblog.txt", "[Database]", true);

		public static DiscordBot Bot;

		static List<Parameter> MainParameters = new List<Parameter>
		{
			new Parameter(Parameter.EType.SingleValue, Parameter.EValueType.String, "credfile", "-",
				"Correct usage: -credfile <file>\n\tSpecifies a credentials file"),
			new Parameter(Parameter.EType.NoValue, Parameter.EValueType.String, "console", "-",
				"Correct usage: -c \n\tEnables the usage of a secondary console", true),
			new Parameter(Parameter.EType.SingleValue, Parameter.EValueType.String, "logfile", "-",
				"Correct usage: -logfile <path to log>\n\tSpecifies a log file to save data to", true),
			new Parameter(Parameter.EType.SingleValue, Parameter.EValueType.String, "dblogfile", "-",
				"Correct usage: -dblogfile <path to log>\b\tSpecifies a log file to save data to", true)
		};

        static void Main(string[] args)
        {
			//Parse params
			Params.Parse(ref MainParameters, args);

			bool bc = true;

			//Look for bad params
			foreach(Parameter p in MainParameters)
			{
				if(p.Value != null && p.Optional != true)
				{
					Console.WriteLine(p.HelpText);
					bc = false;
				}
			}

			//oof
			if (!bc)
				return;

			//Must specify cred file
			if(MainParameters[0].Value == null)
			{
				Console.WriteLine("Must specify credentials file to run the bot!");
				return;
			}

			try
			{
				string fc = File.ReadAllText((string)MainParameters[0].Value);
				Bot = DiscordBot.New(Config.FromFile(fc));
			}
			catch(Exception e)
			{
				ConsoleColor bck = Console.BackgroundColor;
				ConsoleColor fore = Console.ForegroundColor;
				Console.BackgroundColor = ConsoleColor.Red;
				Console.ForegroundColor = ConsoleColor.White;

				Console.WriteLine("Exception occurred:");
				Console.WriteLine("\t {0}", e.GetType());
				Console.WriteLine("\n{0}", e.Message);

				Console.BackgroundColor = bck;
				Console.ForegroundColor = fore;
				return;
			}

			//Run bot
			Bot.Run();
        }

		public static void LogInfo(params object[] list)
		{
			Log.Log(list);
		}

		public static void LogWarning(params object[] list)
		{
			Log.LogWarning(list);
		}

		public static void LogError(params object[] list)
		{
			Log.LogError(list);
		}

		public static void LogFatal(params object[] list)
		{
			Log.LogFatal(list);
		}

		public static void LogSuccess(params object[] list)
		{
			Log.LogSuccess(list);
		}
    }
}
