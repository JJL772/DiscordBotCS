﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS
{
	public static class Settings
	{
	}

	/*
	 * Limits for certain systems
	 */ 
	public static class Limits
	{
		public static uint MaxMessageLength = 2000;
		public static uint MaxBitmapToASCIIWidth = 150;
	}
}
