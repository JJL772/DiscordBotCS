﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Net;

namespace DiscordBotCS.Database.Schema
{
	//
	// PostgreSQL datatype
	//
	public enum ESQLType
	{
		SMALLINT = 0,
		INT,
		BIGINT,
		DECIMAL,
		FLOAT,
		DOUBLE,
		SMALLSERIAL,
		SERIAL,
		BIGSERIAL,
		BOOL,
		TEXT,
		IPADDR,
		UUID,
		DATE,
		MONEY,
		DEFAULT,
	}

	//
	// A field in the data descriptor
	//
	public class SQLField
	{
		public SQLField(ESQLType type)
		{
			Type = type;
			switch(type)
			{
				case ESQLType.SMALLINT:
					Serialize = SerializeShort;
					break;
				case ESQLType.INT:
					Serialize = SerializeInt;
					break;
				case ESQLType.BIGINT:
					Serialize = SerializeLong;
					break;
				case ESQLType.BOOL:
					Serialize = SerializeBool;
					break;
				case ESQLType.FLOAT:
					Serialize = SerializeFloat;
					break;
				case ESQLType.DECIMAL:
					Serialize = SerializeDecimal;
					break;
				case ESQLType.DOUBLE:
					Serialize = SerializeDouble;
					break;
				case ESQLType.TEXT:
					Serialize = SerializeString;
					break;
				case ESQLType.IPADDR:
					Serialize = SerializeIP;
					break;
				case ESQLType.UUID:
					Serialize = SerializeUUID;
					break;
				case ESQLType.DATE:
					Serialize = SerializeTimestamp;
					break;
				case ESQLType.MONEY:
					Serialize = SerializeMoney;
					break;
				case ESQLType.SMALLSERIAL:
					Serialize = SerializeSerial;
					break;
				case ESQLType.SERIAL:
					Serialize = SerializeSerial;
					break;
				case ESQLType.BIGSERIAL:
					Serialize = SerializeSerial;
					break;
				default:
					throw new Exception();
			}
		}

		public ESQLType Type;

		public static string[] TypeStrings =
		{
			"smallint",
			"integer",
			"bigint",
			"decimal",
			"real",
			"double precision",
			"smallserial",
			"serial",
			"bigserial",
			"boolean",
			"text",
			"cidr",
			"uuid",
			"timestamp",
			"money",
			"default",
		};

		public PropertyInfo PropInfo;

		public FieldInfo FieldInfo;

		public delegate string SerializeFn(object newval);

		public SerializeFn Serialize;

		public string Name { get; set; }

		public string GetTypeString()
		{
			return TypeStrings[(int)Type];
		}

		public string GetValue(object obj)
		{
			if (FieldInfo != null)
			{
				return Serialize(FieldInfo.GetValue(obj));
			}
			else if (PropInfo != null)
			{
				return Serialize(PropInfo.GetValue(obj));
			}
			else
				throw new Exception();
		}

		public static string SerializeChar(object val)
		{
			return ((int)val).ToString();
		}

		public static string SerializeShort(object val)
		{
			return ((short)val).ToString();
		}

		public static string SerializeInt(object val)
		{
			return ((int)val).ToString();
		}

		public static string SerializeLong(object val)
		{
			return ((long)val).ToString();
		}

		public static string SerializeString(object val)
		{
			return (string)val;
		}

		public static string SerializeFloat(object val)
		{
			return ((float)val).ToString();
		}

		public static string SerializeDouble(object val)
		{
			return ((double)val).ToString();
		}

		public static string SerializeDecimal(object val)
		{
			return ((decimal)val).ToString();
		}

		public static string SerializeBool(object val)
		{
			return ((bool)val).ToString();
		}

		public static string SerializeDate(object val)
		{
			DateTime v = (DateTime)val;
			return v.ToString();
		}

		public static string SerializeIP(object val)
		{
			return ((IPAddress)val).ToString();
		}

		public static string SerializeTimestamp(object val)
		{
			return "";
		}

		public static string SerializeEnum(object val)
		{
			return "";
		}

		public static string SerializeUUID(object val)
		{
			return ((Guid)val).ToString();
		}

		public static string SerializeMoney(object val)
		{
			return ((float)val).ToString();
		}

		public static string SerializeSerial(object val)
		{
			return ""; //dummy
		}
	}

	public class DataDescriptor
	{
		public string Name;

		public Type Class;

		public List<SQLField> Fields = new List<SQLField>();

		public void Update()
		{

		}

		public T ToClass<T>()
		{
			if (typeof(T) != Class)
				throw new Exception();

			T newClass = Activator.CreateInstance<T>();

			foreach(FieldInfo f in typeof(T).GetFields(BindingFlags.Public))
			{
				foreach(SQLField sf in Fields)
				{
					if(sf.FieldInfo == f)
					{
						//f.SetValue(newClass, sf.)
					}
				}
			}
		}
	}
}
