﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Database.Schema.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	public class SQLTable : Attribute
	{
		public string Name { get; set; }

		public SQLTable()
		{
			//Nothing, just exists
		}
	}
}
