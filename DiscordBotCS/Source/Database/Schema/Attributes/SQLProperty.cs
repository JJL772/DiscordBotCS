﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Database.Schema.Attributes
{
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public class SQLProperty : Attribute
	{
		//
		// Name of this property in the data descriptor
		//
		public string Name { get; set; }

		//
		// Boop... Nothing
		//
		public SQLProperty()
		{

		}
	}
}
