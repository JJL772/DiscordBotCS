﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Database.Schema.Attributes
{
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
	public class SQLIgnore : Attribute
	{
		public SQLIgnore()
		{
			//Nothing
		}
	}
}
