﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using DiscordBotCS.Database.Schema.Attributes;
using DiscordBotCS.Database.Types;
using System.Net;

namespace DiscordBotCS.Database.Schema
{

	public static class SchemaBuilder
	{
		public static List<DataDescriptor> Descriptors = new List<DataDescriptor>();

		//
		// Generic specialization of BuildSchema. This is for compile-time eval
		//
		public static void BuildSchema<T>()
		{
			BuildSchema(typeof(T));
		}

		//
		// Generates an SQL data descriptor for the specified type
		//
		public static void BuildSchema(Type t)
		{
			if (!t.IsClass)
				throw new Exception();

			//
			// Create new descriptor object
			//
			DataDescriptor descriptor = new DataDescriptor();

			//
			// List of SQL fields in the class t
			//
			List<SQLField> fields = new List<SQLField>();

			//
			// Search through all public fields of the class t
			//
			foreach (FieldInfo f in t.GetFields(BindingFlags.Public))
			{
				//
				// Get and test if the SQLProperty attribute is applied to the fields
				//
				SQLProperty prop = (SQLProperty)f.GetCustomAttribute(typeof(SQLProperty));
				if (prop != null)
				{
					//
					// Set the SQL type
					//
					SQLField field = new SQLField(DetermineType(f.FieldType));

					field.FieldInfo = f;
					field.PropInfo = null;

					if (prop.Name != null)
						field.Name = prop.Name;
					else
						field.Name = f.Name;

					fields.Add(field);
				}
			}

			foreach(PropertyInfo p in t.GetProperties(BindingFlags.Public))
			{
				//
				// Get and test if SQLProperty attribute is applied
				//
				SQLProperty prop = (SQLProperty)p.GetCustomAttribute(typeof(SQLProperty));
				if(prop != null)
				{
					//
					// If good set the type
					//
					SQLField field = new SQLField(DetermineType(p.PropertyType));

					field.PropInfo = p;

					if (prop.Name != null)
						field.Name = prop.Name;
					else
						field.Name = p.Name;

					field.FieldInfo = null;

					fields.Add(field);
				}
			}

			//
			// Check for extra name parameter
			//
			SQLTable table = (SQLTable)t.GetCustomAttribute(typeof(SQLTable));

			if (table.Name != null)
				descriptor.Name = table.Name;
			else
				descriptor.Name = t.Name;

			//
			// Finalize
			//
			descriptor.Fields = fields;
			descriptor.Class = t;

			//
			// Finished building schema!
			//
			Descriptors.Add(descriptor);
		}

		//
		// Basically a bunch of IFs to determine the SQL type of the parameter
		//
		private static ESQLType DetermineType(Type val)
		{
			if (val == typeof(char))
			{
				return ESQLType.SMALLINT;
			}
			else if (val == typeof(short))
			{
				return ESQLType.SMALLINT;
			}
			else if (val == typeof(int) || val == typeof(ushort))
			{
				return ESQLType.INT;
			}
			else if (val == typeof(long) || val == typeof(uint))
			{
				return ESQLType.BIGINT;
			}
			else if (val == typeof(float))
			{
				return ESQLType.FLOAT;
			}
			else if (val == typeof(double))
			{
				return ESQLType.DOUBLE;
			}
			else if (val == typeof(decimal))
			{
				return ESQLType.DECIMAL;
			}
			else if (val == typeof(bool))
			{
				return ESQLType.BOOL;
			}
			else if (val == typeof(SmallSerial))
			{
				return ESQLType.SMALLSERIAL;
			}
			else if (val == typeof(Serial))
			{
				return ESQLType.SERIAL;
			}
			else if (val == typeof(BigSerial))
			{
				return ESQLType.BIGSERIAL;
			}
			else if (val == typeof(DateTime))
			{
				return ESQLType.DATE;
			}
			else if (val == typeof(IPAddress))
			{
				return ESQLType.IPADDR;
			}
			else if (val == typeof(string))
			{
				return ESQLType.TEXT;
			}
			else if(val == typeof(Guid))
			{
				return ESQLType.UUID;
			}
			else
				return ESQLType.DEFAULT;
		}
	}
}
