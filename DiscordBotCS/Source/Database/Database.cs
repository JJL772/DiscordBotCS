﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.Types;
using MySql.Data.Common;

namespace DiscordBotCS.Database
{
	//Represents a class and all of its contained properties
	public class ClassDescriptor
	{

	}
	public class Database
	{

		//Static constructor, called once when a new db object is created
		static Database()
		{

		}

		public void Connect(string ip, string port, string pw)
		{

		}

		public void SelectDatabase(string db)
		{

		}

		public bool DatabaseExists(string db)
		{
			return true;
		}

		public bool TableExists(string tblname)
		{
			return false;
		}

		public void CreateTable<T>(string tblname)
		{

		}

		public void DropTable(string tblname)
		{

		}

		public void Insert<T>(string tblname, T data)
		{

		}

		public void Delete<T>(string tblname, T cond)
		{

		}

		public void Delete(string tblname, string cond)
		{

		}

		public T[] Fetch<T>(string tblname, T cond)
		{
			return null;
		}

		public T[] Fetch<T>(string tblname, string param)
		{
			return null;
		}

		public void Update<T>(string tblname, T cond, T _new)
		{

		}

		public void Update<T>(string tblname, string cond, T _new)
		{

		}


	}
}
