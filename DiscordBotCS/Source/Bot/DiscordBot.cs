﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Commands;
using DSharpPlus.Entities;
using DSharpPlus;
using DSharpPlus.VoiceNext;
using DiscordBotCS.Commands.CommandLine;
using Imgur.API.Authentication;
using Imgur.API.Authentication.Impl;
using Imgur.API.Endpoints;
using Imgur.API.Endpoints.Impl;
using System.Threading;
using System.Net;
using DiscordBotCS.Util.Data.Protobuf;
using DiscordBotCS.Util.Data;
using PortableSteam;
using DiscordBotCS.Util.Data;

namespace DiscordBotCS.Bot
{
	public class DiscordBot
	{
		/* Public props */
		public CommandRegistry Commands;
		public DiscordClient Client;
		public Random RandomEngine = new Random(DateTime.Now.Millisecond);
		public List<VoiceNextClient> VoiceClients = new List<VoiceNextClient>();
		//public CommandRegistry CommandLineCommands = new CommandRegistry();
		public ChatTriggerHandler ChatTriggerHandle = new ChatTriggerHandler();
		public IApiClient ImgurClient = null;
		public IGalleryEndpoint GalleryEndpoint = null;
		public IImageEndpoint ImageEndpoint = null;
		public DiscordGame CurrentGame = null;
		public BotCommandLine CommandLine = null; //Assigned in constructor
		public List<Setting> Settings = new List<Setting>();
		public bool IsCmdLine = false;
		public WebClient WebClient = new WebClient();
		public UserList UserList = null;
		public Timer SaveTimer;

		/* Private props */
		private bool IsEnabled = false;
		private Config Config;

		public static DiscordBot New(Config config)
		{
			DiscordBot bot = new DiscordBot(config);
			return bot;
		}

		public void Run()
		{
			BotExec().Wait();
		}

		private DiscordBot(Config config)
		{
			Config = config;

			CurrentGame = new DiscordGame("Working on v1.0!");

			UserList = UserLoader.LoadFromFile();

			/*
			Save timer
			TODO: Remove
			*/
			SaveTimer = new Timer(SaveTimerCallback, null, 100000, (1000 * 60 * 10));

			/*
			Initialize voice clients list
			*/
			VoiceClients = new List<VoiceNextClient>();
			//Commands = new CommandRegistry();

			/*
			Imgur API init
			*/
			ImgurClient = new ImgurClient(config.ImgurAPIKey);
			GalleryEndpoint = new GalleryEndpoint(ImgurClient);
			ImageEndpoint = new ImageEndpoint(ImgurClient);
		}

		private async Task fuckyou() { }

		public async Task HandleCmd()
		{
			while (true) { }
		}

		private void SaveTimerCallback(Object info)
		{
			//Console.WriteLine("Saving data...");
			UserLoader.SaveToFile(UserList);
		}

		public async Task BotExec()
		{
			//Util.DiscordUtils.ReadTokenFromFile("steamtoken.txt", out SteamWebAPIToken);

			/*
			New discord client
			*/
			Client = new DiscordClient(new DiscordConfiguration
			{
				Token = Config.DiscordClientSecret,
				TokenType = TokenType.Bot,
				AutoReconnect = true,
			});

			/*
			On ready event
			*/
			Client.Ready += async e =>
			{
				await fuckyou();

				Application.Log.Log("Initialized and ready for action!");

				await Client.UpdateStatusAsync(CurrentGame);

				Application.Log.Log("Updated status.");

				if(!IsEnabled)
				{
					IsEnabled = true;
					SaveTimer.Change(1, (60 * 1000 * 10));
				}
			};

			/*
			Called when a guild member is updated
			*/
			Client.GuildMemberUpdated += async e =>
			{
				await fuckyou();
				UserLoader.UpdateUsers(this);
			};

			/*

			*/
			Client.MessageCreated += async e =>
			{
				await fuckyou();
				if (e.Message.Author.IsBot)
					return;

				ChatTriggerHandle.ExecuteChatTrigger(e.Message);

				//TODO: Add new command parse system

				//Commands.ParseString(e.Message, this);
				CommandRegistry.ParseMessage(e.Message.Content, e);
			};

			Client.GuildAvailable += async e =>
			{
				await fuckyou();

				Application.Log.Log("Initialized for guild: ", e.Guild.Name);

				UserLoader.UpdateUsers(this);
			};

			Client.GuildDeleted += async e =>
			{
				await fuckyou();

				Application.Log.Log("Removed from guild: ", e.Guild.Name);
			};

			await Client.ConnectAsync();

			//So we dont fall out of
			await Task.Delay(-1);
		}
	}
}
