﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;

namespace DiscordBotCS.Bot
{
	/// <summary>
	/// Handles chat triggers, which are basically functions called when a specific message is sent.
	/// </summary>
	public class ChatTriggerHandler
	{
		/**
		 * Maps chat strings to selective responses
		 */ 
		public Dictionary<string, string> ChatTriggers = new Dictionary<string, string>();

		/**
		 * Different swaps for some parameters
		 */ 
		public const string UserNameSwap = "%USERNAME%";
		public const string HourSwap = "%HOUR%";
		public const string MinuteSwap = "%MINUTE%";
		public const string SecondSwap = "%SECOND%";
		public const string MillisecondSwap = "%MILLISECOND%";
		public const string MonthSwap = "%MONTH%";
		public const string DaySwap = "%DAY%";
		public const string YearSwap = "%YEAR%";
		public const string EpochTimeSwap = "%UNIXTIME%";
		public const string UserIDSwap = "%USERID%";
		public const string MentionStringSwap = "%MENTIONSTRING%";

		public ChatTriggerHandler()
		{

		}

		public bool ContainsChatTrigger(string message)
		{
			return ChatTriggers.ContainsKey(message);
		}

		public void ExecuteChatTrigger(DiscordMessage msg)
		{
			if(ContainsChatTrigger(msg.Content))
			{
				string response = "";
				ChatTriggers.TryGetValue(msg.Content, out response);
				response = response.Replace(UserNameSwap, msg.Author.Username);
				response = response.Replace(HourSwap, DateTime.Now.Hour.ToString());
				response = response.Replace(MinuteSwap, DateTime.Now.Minute.ToString());
				response = response.Replace(SecondSwap, DateTime.Now.Second.ToString());
				response = response.Replace(MillisecondSwap, DateTime.Now.Millisecond.ToString());
				response = response.Replace(MonthSwap, DateTime.Now.Month.ToString());
				response = response.Replace(DaySwap, DateTime.Now.Day.ToString());
				response = response.Replace(YearSwap, DateTime.Now.Year.ToString());
				response = response.Replace(UserIDSwap, msg.Author.Id.ToString());
				response = response.Replace(MentionStringSwap, msg.Author.Mention);
				msg.RespondAsync(response);
			}
		}
	}
}
