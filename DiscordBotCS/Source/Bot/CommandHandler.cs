﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace DiscordBotCS.Bot
{
	public class Setting
	{
		public string Name;
		public object Value;
		public ESettingType Type = ESettingType.String;
		public Setting(string name)
		{
			Name = name;
		}
		public bool IsValid()
		{
			return (Name != null && Value != null);
		}
	}

	public class BotCommandLine
	{
		private Dictionary<string, Func<DiscordBot, List<string>, bool>> CMDLineCMDs = new Dictionary<string, Func<DiscordBot, List<string>, bool>>();
		public Dictionary<string, string> Settings = new Dictionary<string, string>();
		public DiscordBot BotParent;

		public BotCommandLine(DiscordBot bot)
		{
			BotParent = bot;
		}

		/*
		 * Command loop
		 */ 
		public void CommandLoop()
		{
			while(true)
			{
				List<string> input1 = Console.ReadLine().Split(' ').ToList();
				if(input1.Count >= 1)
				{
					foreach(KeyValuePair<string, Func<DiscordBot, List<string>, bool>> c in CMDLineCMDs)
					{
						if (input1[0] == c.Key)
							c.Value(BotParent, input1);
					}
				}
			}
		}

		public void AddCommand(string cmd, Func<DiscordBot, List<string>, bool> fnc)
		{
			CMDLineCMDs.Add(cmd, fnc);
		}

		public void AddCommand(string[] cmd, Func<DiscordBot, List<string>, bool> fnc)
		{
			foreach(string c in cmd)
			{
				CMDLineCMDs.Add(c, fnc);
			}
		}

		public bool HasCommand(string cmd)
		{
			foreach(KeyValuePair<string, Func<DiscordBot, List<string>, bool>> kv in CMDLineCMDs)
			{
				if (kv.Key == cmd)
					return true;
			}
			return false;
		}

		public void RemoveCommand(string cmd)
		{
			foreach(KeyValuePair<string, Func<DiscordBot, List<string>, bool>> kv in CMDLineCMDs)
			{
				if(cmd == kv.Key)
				{
					CMDLineCMDs.Remove(cmd);
				}
			}
		}
	}

	public static class Commands
	{
		public static bool HelpCommand(DiscordBot bot, List<string> args)
		{
			
			return true;
		}

		public static bool GetSettingCommand(DiscordBot bot, List<string> args)
		{
			if (args.Count >= 2)
			{
				foreach(Setting s in bot.Settings)
				{
					if(s.Name == args[1])
					{
						Console.WriteLine(s.Value.ToString());
						return true;
					}
				}
			}
			return false;
		}

		public static bool SetSettingCommand(DiscordBot bot, List<string> args)
		{
			if (args.Count >= 4)
			{
				foreach(Setting s in bot.Settings)
				{
					if(s.Name == args[1])
					{
						switch(args[2].ToLower())
						{
							case "int":
								int i = 0;
								int.TryParse(args[3], out i);
								s.Value = i;
								s.Type = ESettingType.Int;
								break;
							case "float":
								float f = 0.0f;
								float.TryParse(args[3], out f);
								s.Value = f;
								s.Type = ESettingType.Float;
								break;
							case "double":
								double d = 0.0d;
								double.TryParse(args[3], out d);
								s.Value = d;
								s.Type = ESettingType.Double;
								break;
							case "bool":
								bool b = false;
								bool.TryParse(args[3], out b);
								s.Value = b;
								s.Type = ESettingType.Bool;
								break;
							case "long":
								long l = 0;
								long.TryParse(args[3], out l);
								s.Value = l;
								s.Type = ESettingType.Long;
								break;
							default:
								s.Value = args[3];
								s.Type = ESettingType.String;
								break;
						}
						Console.WriteLine("Set " + args[1] + " to " + args[3]);
						return true;
					}
				}
				Console.WriteLine("That setting does not exist. Would you like to create a new setting? (y/n)");
				string inp = Console.ReadLine().ToLower();
				if(inp == "y")
				{
					Setting s = new Setting(args[1]);
					switch(args[2].ToLower())
					{
						case "int":
							int i = 0;
							int.TryParse(args[3], out i);
							s.Type = ESettingType.Int;
							s.Value = i;
							break;
						case "float":
							float f = 0.0f;
							float.TryParse(args[3], out f);
							s.Type = ESettingType.Float;
							s.Value = f;
							break;
						case "long":
							long l = 0;
							long.TryParse(args[3], out l);
							s.Type = ESettingType.Long;
							s.Value = l;
							break;
						case "double":
							double d = 0.0d;
							double.TryParse(args[3], out d);
							s.Type = ESettingType.Double;
							s.Value = d;
							break;
						case "bool":
							bool b = false;
							bool.TryParse(args[3], out b);
							s.Type = ESettingType.Bool;
							s.Value = b;
							break;
						default:
							s.Type = ESettingType.String;
							s.Value = args[3];
							break;
					}
					Console.WriteLine("Set " + args[1] + " to " + args[3]);
					return true;
				}
			}
			Console.WriteLine("Incorrect syntax! set <setting> <int|float|double|bool|long|string> <val>");
			return false;
		}

		public static bool RemoveSettingCommand(DiscordBot bot, List<string> args)
		{
			if(args.Count >= 2)
			{
				foreach(Setting s in bot.Settings)
				{
					if(s.Name == args[1])
					{
						bot.Settings.Remove(s);
						return true;
					}
				}
				Console.WriteLine("Could not find setting with specified name.");
				return false;
			}
			Console.WriteLine("Incorrect syntax! rem <setting>");
			return false;
		}
	}
}
