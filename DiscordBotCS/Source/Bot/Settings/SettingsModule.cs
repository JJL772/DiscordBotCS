﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Bot
{
	public enum ESettingType
	{
		Char,
		Short,
		UShort,
		Int,
		UInt,
		Long,
		ULong,
		Float,
		Double,
		String,
		Boolean,
		Bool,
	}

	public struct SettingEntry
	{
		string Name;
		ESettingType SettingType;
		Object Value;

		public bool IsValid() { return Value != null; }

		public char GetAsChar()
		{
			if (IsValid())
			{
				if (Value.GetType() == typeof(char))
					return (char)Value;
			}
			return '\0';
		}

		public Int16 GetAsInt16()
		{
			if (IsValid())
			{
				if (Value.GetType() == typeof(Int16))
					return (Int16)Value;
			}
			return -1;
		}

		public UInt16 GetAsUInt16()
		{
			if (IsValid())
			{
				if (Value.GetType() == typeof(UInt16))
					return (UInt16)Value;
			}
			return 0;
		}

		public Int32 GetAsInt32()
		{
			if (IsValid())
			{
				if (Value.GetType() == typeof(Int32))
					return (Int32)Value;
			}
			return -1;
		}

		public UInt32 GetAsUInt32()
		{
			if (IsValid())
			{
				if (Value.GetType() == typeof(UInt32))
					return (UInt32)Value;
			}
			return 0;
		}

		public Int64 GetAsInt64()
		{
			if (IsValid())
			{
				if (Value.GetType() == typeof(Int64))
					return (Int64)Value;
			}
			return -1;
		}

		public UInt64 GetAsUInt64()
		{
			if (IsValid())
			{
				if (Value.GetType() == typeof(UInt64))
					return (UInt64)Value;
			}
			return 0;
		}

		public float GetAsFloat()
		{
			if(IsValid())
			{
				if (Value.GetType() == typeof(float))
					return (float)Value;
			}
			return -1.0f;
		}

		public double GetAsDouble()
		{
			if(IsValid())
			{
				if (Value.GetType() == typeof(double))
					return (double)Value;
			}
			return -1.0;
		}

		public string GetAsString()
		{
			if(IsValid())
			{
				if (Value.GetType() == typeof(string))
					return (string)Value;
			}
			return "";
		}

		public bool GetAsBool()
		{
			if(IsValid())
			{
				if (Value.GetType() == typeof(bool))
					return (bool)Value;
			}
			return false;
		}
	}

	public class SettingsModule
	{
		List<SettingEntry> SettingsList = new List<SettingEntry>();


	}
}
