﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;

namespace DiscordBotCS.Commands
{
	public class RemoveMessagesCommand : BaseCommand
	{
		public RemoveMessagesCommand()
		{
			Aliases = new List<string>();
			Aliases.Add("$deletemessages");
			Aliases.Add("$dm");
			HelpText = "Removes all bot messages from the channel";
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			await Message.RespondAsync("Cleaning all bot messages... (This may take a while) *Cue Jeopardy theme song*");
			List<DiscordMessage> msgs = (await Message.Channel.GetMessagesAsync()).ToList();
			foreach(DiscordMessage msg in msgs)
			{
				if(msg.Author.Id == Application.Bot.Client.CurrentUser.Id)
				{
					await msg.DeleteAsync();
				}
			}
		}
	}
}
