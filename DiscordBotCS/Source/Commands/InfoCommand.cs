﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using DiscordBotCS;

namespace DiscordBotCS.Commands
{
	public class InfoCommand : BaseCommand
	{
		public InfoCommand()
		{
			Aliases = new List<string>();
			Aliases.Add("$info");
			HelpText = "Displays info on the bot";
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot Bot)
		{
			var embed = new DiscordEmbedBuilder();
			embed.Title = "**Memes and Porn, Inc. Info**";
			foreach(var pair in Data.InfoCommandFields)
			{
				embed.AddField(pair.Key, pair.Value);
			}
			await Message.RespondAsync(embed: embed);
		}
	}
}
