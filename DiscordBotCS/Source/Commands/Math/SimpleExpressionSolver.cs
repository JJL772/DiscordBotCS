﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;

namespace DiscordBotCS.Commands.Math
{
	public class SimpleExpressionSolver : BaseCommand
	{
		public SimpleExpressionSolver()
		{
			Aliases.Add("$solve");
			IsHidden = true;
			HelpText = "Solves a simple mathematical expression.";
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			
		}
	}
}
