﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;

namespace DiscordBotCS.Commands.Math
{
	public class SineWaveCommand : BaseCommand
	{
		public SineWaveCommand()
		{
			Aliases = new List<string>();
			Aliases.Add("$sinewave");
			HelpText = "Prints a sine wave with the specified character";
			IsHidden = true;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			char c = Args.Length == 0 ? '.' : Args[0][0];
			string finalStr = "```\n";
			for(double i = 0; i < 20 * System.Math.PI; i += (System.Math.PI/2))
			{
				finalStr += "\t";
				double spaces = System.Math.Sin(i) + 5;
				for(double f = 0; f <= spaces; f++)
				{
					finalStr += " ";
				}
				finalStr += c;
				finalStr += "\n";
			}
			finalStr += "\n```";
			await Message.RespondAsync(finalStr);
		}
	}
}
