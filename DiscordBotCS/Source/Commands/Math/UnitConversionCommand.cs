﻿using DiscordBotCS.Bot;
using DiscordBotCS.Commands;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Util;

namespace DiscordBotCS.Commands.Math
{
	

	public class UnitConversionsCommand : BaseCommand
	{
		public UnitConversionsCommand()
		{
			Aliases.Add("$convertion_list");
			Aliases.Add("$conv_list");
			HelpText = "Lists all conversions";
			IsHidden = false;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{

		}
	}
	public class UnitConverterCommand : BaseCommand
	{
		public UnitConverter UnitConverter = new UnitConverter();

		public UnitConverterCommand()
		{
			Aliases.Add("$convert");
			Aliases.Add("$conv");
			HelpText = "Converts units. For a list of conversion ids, use $conv_list or $conversion_list";
			IsHidden = false;

			//Setup all of our unit conversions

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.MileAliases, Data.FeetAliases, 5820.0f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.MileAliases, Data.YardAliases, 1760.0f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.MileAliases, Data.KilometerAliases, 1.60934f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.MileAliases, Data.CentimeterAliases, 160934.0f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.BTUAliases, Data.JoulesAliases, 1055.06f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.JoulesAliases, Data.CalorieAliases, 4.184f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.JoulesAliases, Data.KCalAliases, 0.000239006f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.KCalAliases, Data.CalorieAliases, 1000.0f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.BTUAliases, Data.CalorieAliases, 252.164f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.BTUAliases, Data.KCalAliases, 0.252164f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.InchAliases, Data.CentimeterAliases, 2.54f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.InchAliases, Data.MeterAliases, 0.0254f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.InchAliases, Data.KilometerAliases, 2.54e-5f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.MeterAliases, Data.YardAliases, 1.09361f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.KilometerAliases, Data.YardAliases, 1093.61f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.AtmAliases, Data.BarAliases, 1.01325f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.AtmAliases, Data.PSIAliases, 14.6959f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.AtmAliases, Data.mmHGAliases, 760.0f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.PSIAliases, Data.BarAliases, 0.0689476f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.PSIAliases, Data.mmHGAliases, 51.7149f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.BarAliases, Data.mmHGAliases, 750.062f));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.DegreeAliases, Data.RadianAliases, 0.0174533f));

			//wELl wE nEeD fUNcTiOnS SoMeTiMeS () =>{ return "AHHHHHHHHHHHHHEHHHHHHHHHEHEHEHEHHHEHHEHEHHEHHHHHHHH"; };
			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.FAliases, Data.CelsiusAliases, (bool b, float f) => 
			{
				if (!b)
					return (f - 32.0f) * (5.0f / 9.0f);
				else
					return f * (9.0f / 5.0f) + 32.0f;
			}));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.CelsiusAliases, Data.KelvinAliases, (bool b, float f) =>
			{
				if (!b)
					return f + 273.0f;
				else
					return f - 273.0f;
			}));

			UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.FAliases, Data.KelvinAliases, (bool b, float f) =>
			{
				if (!b)
					return (f - 32.0f) * (5.0f / 9.0f) + 273;
				else
					return ((f - 273) * (9.0f / 5.0f) + 32.0f);
			}));

			//UnitConverter.AddConversionEntry(new UnitConversionEntry(Data.))
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			if(Args == null || Args[0] == null)
			{
				await Message.RespondAsync("Not enough operands!");
				return;
			}
			else if(Args.Length < 3)
			{
				DiscordUtils.PrintArgumentError(Message.Content, (uint)Message.Content.Length, "$convert <unitFrom> <unitTo> <units>", Message);
				return;
			}

			string Unit1 = Args[0].ToLower();
			string Unit2 = Args[1].ToLower();
			double Ammt = 0.0f;
			if (!Double.TryParse(Args[2], out Ammt))
			{
				await Message.RespondAsync("Failed to parse number, please use an actual number. Not hex, not binary, not scientific notation, an *actual* number.");
				return;
			}

			if(!UnitConverter.ContainsEntry(Unit1, Unit2))
			{
				await Message.RespondAsync("Unrecognized units in parameter list.");
				return;
			}

			await Message.RespondAsync(Ammt.ToString() + Unit1 + " -> " + UnitConverter.ConvertUnits(Unit1, Unit2, (float)Ammt) + Unit2);
		}
	}
}
