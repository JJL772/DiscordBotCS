﻿using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace DiscordBotCS.Commands.Math
{
	public class EvaluateCommand : BaseCommand
	{
		public EvaluateCommand()
		{
			Aliases.Add("$eval");
			HelpText = "Evaluates a simple expression. Can't handle things such as: summation, trig or algebraic expressions. Strictly for simple numbers.";
			IsHidden = false;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			if (Args == null)
				await Message.RespondAsync("You must have a valid expression!");
			DataTable t = new DataTable();
			try
			{
				t.Columns.Add("Eval", typeof(double), Message.Content.Replace("$eval", ""));
				t.Rows.Add(0);
			}
			catch(Exception e)
			{
				await Message.RespondAsync("```\n" + e.Message + "\n```");
				return;
			}
			
			await Message.RespondAsync(((double)t.Rows[0]["Eval"]).ToString());
		}
	}
}
