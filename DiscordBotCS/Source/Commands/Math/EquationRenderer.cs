﻿using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using WpfMath;
using System.Drawing;
using System.IO;
using System.Windows.Media;

namespace DiscordBotCS.Commands.Math
{
	public class EquationRenderer : BaseCommand
	{
		private TexFormulaParser Parser;

		public EquationRenderer()
		{
			Aliases.Add("$equation");
			HelpText = "Renders a LaTeX equation.";
			IsHidden = false;
			Parser = new TexFormulaParser();
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			String equation = Message.Content.Replace("$equation", "");
			try
			{
				TexFormula formula = Parser.Parse(equation);
				formula.SetBackground(new SolidColorBrush(System.Windows.Media.Color.FromRgb(255,255,255)));
				TexRenderer renderer = formula.GetRenderer(TexStyle.Display, 20.0f, "Arial");
				BitmapSource s = renderer.RenderToBitmap(renderer.Box.Width, renderer.Box.Height);

				//Read directly from memory using a memory stream
				FileStream stream = new FileStream("Data/equation.png", FileMode.CreateNew);
				PngBitmapEncoder encoder = new PngBitmapEncoder();
				encoder.Frames.Add(BitmapFrame.Create(s));
				encoder.Save(stream);
				stream.Flush();
				await Message.RespondWithFileAsync(stream, "equation.png");
			}
			catch(Exception e)
			{
				await Message.RespondAsync("Error parsing equation! " + e.Message);
			}
		}
	}
}
