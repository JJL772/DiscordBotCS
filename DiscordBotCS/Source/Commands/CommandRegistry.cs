﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using System.Threading;
using System.Reflection;
using DiscordBotCS.Commands.Attributes;
using DiscordBotCS.Util;
using DiscordBotCS.Util.Data;
using System.IO;
using Newtonsoft.Json;
using DSharpPlus.EventArgs;

using CommandAttribute = DiscordBotCS.Commands.Attributes.Command;


namespace DiscordBotCS.Commands
{
	public class CommandWrapper
	{
		//public Action<List<CommandParameterBase>, string> RunCmd { get; set; }

		public delegate void RunCmdFn(List<CommandParameterBase> plist, string cmdline);

		public string Name;

		public string ShortHelp;

		public string LongHelp;

		public bool Dev;

		public bool Hidden;

	}

	public class CommandRegistry
	{
		//Uncomment for old system
		//public List<BaseCommand> Commands;

		public static List<CommandDescriptor> Commands = new List<CommandDescriptor>();

		private CommandRegistry()
		{

		}

		/*

		Registers all commands in the command list file

		*/
		public static void LoadCommandList(string file)
		{
			StreamReader fs;

			//Try to read file
			try
			{
				fs = new StreamReader(File.OpenRead(file));
			}
			catch(Exception e)
			{
				Application.Log.LogFatal("Unable to read command list file: " + file + ". Exiting...");
				throw new Exception();
			}

			//Read to end
			string jsondata = fs.ReadToEnd();
			fs.Close();

			//Parse
			JSONCommandList list = JsonConvert.DeserializeObject<JSONCommandList>(jsondata);

			//oof
			if (list == null)
			{
				Application.Log.LogFatal("Unable to read command list file: " + file + ". Exiting...");
				throw new Exception();
			}

			//Set command list
			Commands = (List<CommandDescriptor>)list.CommandList;

			//For each of the descriptors parsed, run
			foreach(CommandDescriptor cmd in Commands)
			{
				//For each of the classes in this assembly
				foreach(Module m in Assembly.GetCallingAssembly().GetModules())
				{
					//Get the command attribute
					CommandAttribute attr = (CommandAttribute)m.GetCustomAttribute(typeof(CommandAttribute));

					//If the attribute is good, continue
					if(attr != null)
					{
						//If the name matches up
						if((attr.Name != null && attr.Name == cmd.Name) || (m.Name == cmd.Name))
						{
							//Set the command class type
							cmd.CommandClass = m.GetType();

							//Get the method info for the Run function
							MethodInfo method = m.GetMethod("Run", new Type[]{
								typeof(IList<string>),
								typeof(string),
								typeof(CommandDescriptor),
							});

							//Finally create a delegate, if we cant create a delegate there must be something wrong!
							if (method != null)
								cmd.Run = (CommandDescriptor.RunFn)method.CreateDelegate(typeof(CommandDescriptor.RunFn));
							else
								Application.Log.LogWarning("Unable to register command ", cmd.Name, " due to an invalid Run function.");
						}
					}
				}
			}

		}

		public static void ParseMessage(string message, MessageCreateEventArgs e)
		{
			//TODO: IMPLEMENT THIS

			if(message.StartsWith("$") && message.Length > 1)
			{
				//Prepare messsage
				message = message.Remove(0, 1);
				string[] msg = message.Split(' ');

				//Search for fitting command
				foreach(CommandDescriptor desc in Commands)
				{
					foreach(string s in desc.Aliases)
					{
						if(s == msg[0])
						{
							if (desc.EnforceParams)
							{
								
							}
							else
							{

							}
						}
					}
				}
			}

		}

		/* Uncomment if you wish to use the crappy middle-system
		public void RegisterCommands()
		{
			Assembly assembly = Assembly.GetCallingAssembly();

			foreach(Module module in assembly.GetModules())
			{
				foreach(object o in module.GetCustomAttributes(false))
				{
					if(o.GetType() == typeof(Command))
					{
						//Command should have static Run function
						MethodInfo fn = module.GetMethod("Run", new Type[]
						{
							typeof(List<CommandParameterBase>),
							typeof(string),
						});

						if(fn != null && fn.ReturnType == typeof(void) && fn.IsPublic && fn.IsStatic)
						{
							fn.CreateDelegate(typeof(CommandWrapper.RunCmdFn));
						}
					}
				}
			}
		}
		*/
		/*
		public void RegisterCommand(BaseCommand cmd)
		{
			//Some safety checks
			if (cmd == null || cmd.Aliases[0] == null || cmd.HelpText == null)
			{
				Console.Write("Failed to register command, one or more null parameters!");
				return;
			}

			if(!ContainsCommand(cmd))
			{
				Commands.Add(cmd);
			}
			else
			{
				Console.WriteLine("Failed to register command: " + cmd.Aliases[0] + " as it already exists in the registry");
			}
		}

		public void RemoveCommand(BaseCommand cmd)
		{
			foreach(BaseCommand c in Commands)
			{
				if(c == cmd)
				{
					Commands.Remove(c);
				}
			}
		}

		public bool ContainsCommand(BaseCommand cmd)
		{
			foreach(BaseCommand c in Commands)
			{
				foreach(string alias in cmd.Aliases)
				{
					foreach(string alias1 in c.Aliases)
					{
						if (alias == alias1)
							return true;
					}
				}
			}
			return false;
		}

		public bool ContainsCommand(string Command)
		{
			foreach(BaseCommand cmd in Commands)
			{
				foreach(string alias in cmd.Aliases)
				{
					if (alias == Command)
						return true;
				}
			}
			return false;
		}

		public BaseCommand GetCommandByName(string Command)
		{
			foreach(BaseCommand cmd in Commands)
			{
				foreach(string alias in cmd.Aliases)
				{
					if (Command == alias)
						return cmd;
				}
			}
			return null;
		}

		public bool StringContainsCommand(string str)
		{
			foreach(BaseCommand cmd in Commands)
			{
				foreach(string alias in cmd.Aliases)
				{
					if (str.Contains(alias))
						return true;
				}
			}
			return false;
		}

		public void ParseString(DiscordMessage msg, DiscordBot bot)
		{
			if(StringContainsCommand(msg.Content))
			{
				RunCommand(msg, bot);
			}
		}

		public void RunCommand(DiscordMessage msg, DiscordBot bot)
		{
			char[] sep = ' '.ToString().ToCharArray();
			List<string> Command = msg.Content.Split(sep).ToList();
			foreach(BaseCommand cmd in Commands)
			{
				foreach(string alias in cmd.Aliases)
				{
					if(Command.Contains(alias))
					{
						Command.RemoveAt(Command.IndexOf(alias));

						//To get all our functions to work good, we need to run them asynchronously, so no awaiting here!
						CommandWrapperUtil.RunCommand(cmd.Run, Command.ToArray(), msg, bot);
					}
				}
			}
		}	
		*/
	}

}
