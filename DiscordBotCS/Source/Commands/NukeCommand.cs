﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DiscordBotCS.Util;
using DSharpPlus.Entities;
using System.Threading;

namespace DiscordBotCS.Commands
{
	public class NukeCommand : BaseCommand
	{
		public NukeCommand()
		{
			Aliases = new List<string>();
			Aliases.Add("$nuke");
			HelpText = "Nukes something...";
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			Random rand = bot.RandomEngine;

			if (Args.Length <= 0)
			{
				DiscordUtils.PrintArgumentError(Message.Content, (uint)Message.Content.Length, "$nuke <string>", Message);
				return;
			}

			string finalString = "";
			foreach (string arg in Args)
			{
				finalString += arg;
			}

			var emote = DiscordEmoji.FromName(Application.Bot.Client, ":bomb:");

			await Message.RespondAsync("Nuking " + finalString + " in");
			await Message.RespondAsync("3...");
			await Task.Delay(1000);
			await Message.RespondAsync("2...");
			await Task.Delay(1000);
			await Message.RespondAsync("1...");
			await Message.RespondAsync(embed: new DiscordEmbedBuilder()
			{
				Title = $"{emote} {emote} {emote} {emote}",
				ImageUrl = Data.NukeImgs[rand.Next(0, Data.NukeImgs.Length - 1)],
			});
		}
	}
}
