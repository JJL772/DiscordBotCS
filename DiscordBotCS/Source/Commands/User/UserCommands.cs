﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using User_ = DiscordBotCS.Util.Data.Protobuf.User;
using DiscordBotCS.Util.Data;
using DiscordBotCS.Util;
using System.Diagnostics;

//FIX PLS
namespace DiscordBotCS.Commands.User
{
	public class UpvoteUserCommand : BaseCommand
	{
		public UpvoteUserCommand()
		{
			Aliases.Add("$upvote");
			HelpText = "Upvotes the specified user.";
			IsHidden = true;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			if (((DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds - UserUtil.GetUserByID(Message.Author.Id, Message.Channel.Guild, bot).LastDownvoteTime) < (60 * 60 * 1000))
			{
				await Message.RespondAsync("You can only upvote someone once per hour!");
				return;
			}

			DiscordGuild g = Message.Channel.Guild;
			foreach(DiscordMember member in g.Members)
			{
				string[] str = Message.Content.Split('@');
				foreach(string s in str)
				{
					ulong ID_ = 0;
					string fd = s.Replace('>', ' ');
					fd = s.Replace('<', ' ');
					fd = s.TrimEnd(' ');
					Console.WriteLine("First one: " + fd);
					Console.WriteLine("Second one: " + member.Id);
					if (ID_ == member.Id && ulong.TryParse(fd, out ID_))
					{
						foreach (User_ u in bot.UserList.Users)
						{
							if (u.ID == member.Id)
							{
								u.Points++;
								await Message.RespondAsync("Upvoted user.");
								TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
								UserUtil.GetUserByID(Message.Author.Id, g, bot).LastDownvoteTime = (ulong)t.TotalMilliseconds;
								return;
							}
						}
					}
				}
			}
			await Message.RespondAsync("Invalid user :(");
			return;
		}
	}

	public class DownvoteUserCommand : BaseCommand
	{
		public DownvoteUserCommand()
		{
			Aliases.Add("$downvote");
			HelpText = "Downvotes the specified user.";
			IsHidden = true;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			if(((DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalMilliseconds - UserUtil.GetUserByID(Message.Author.Id, Message.Channel.Guild, bot).LastDownvoteTime) < (60 * 60 * 1000))
			{
				await Message.RespondAsync("You can only downvote someone once per hour!");
				return;
			}

			DiscordGuild g = Message.Channel.Guild;
			foreach (DiscordMember member in g.Members)
			{
				foreach (string s in Args)
				{
					if (s == member.Mention)
					{
						foreach (User_ u in bot.UserList.Users)
						{
							if (u.ID == member.Id)
							{
								u.Points--;
								await Message.RespondAsync("Downvoted user.");
								TimeSpan t = DateTime.UtcNow - new DateTime(1970, 1, 1);
								UserUtil.GetUserByID(Message.Author.Id, g, bot).LastDownvoteTime = (ulong)t.TotalMilliseconds;
								return;
							}
						}
					}
				}
			}
			await Message.RespondAsync("Invalid user :(");
			return;
		}
	}
}
