﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using DiscordBotCS.Util;

namespace DiscordBotCS.Commands
{
	public class PokeCommand : BaseCommand
	{
		public PokeCommand()
		{
			Aliases = new List<string>();
			Aliases.Add("$poke");
			HelpText = "Use this command to poke somebody";
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			List<string> parts = Message.Content.Split(' '.ToString().ToCharArray(), 2).ToList();

			if (parts.Count > 1)
			{
				await Message.DeleteAsync();
				await Message.RespondAsync(Message.Author.Mention + " poked you " + parts[1]);
			}
			else
			{
				DiscordUtils.PrintArgumentError(Message.Content, (uint)Message.Content.Length, "$poke <user>", Message);
				return;
			}
		}
	}
}
