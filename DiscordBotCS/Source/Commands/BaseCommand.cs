﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DiscordBotCS.Bot;

namespace DiscordBotCS.Commands
{
	public class BaseCommand
	{
		public List<string> Aliases = new List<string>();
		public string HelpText = "";
		public string ExtendedHelpText = "";
		public bool UseMultithreading = false;
		public bool IsHidden = false;

		public virtual void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			Message.RespondAsync("[public class]BaseCommand [Run(string[], DiscordMessage, DiscordBot)]: This command has not been implemented yet. Whoever wrote this is a fucking dingus. FIX YOUR SHIT");
		}
	}
}
