﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using DiscordBotCS.Util;

namespace DiscordBotCS.Commands.Steam
{
	public class SteamProfileCommand : BaseCommand
	{
		public SteamProfileCommand()
		{
			Aliases = new List<string>();
			Aliases.Add("$steamprofile");
			Aliases.Add("$steamtest");
			HelpText = "Displays various info about someone's Steam profile.";
			IsHidden = true;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			if(!DiscordUtils.IsUserAdmin(DiscordUtils.DiscordUserToMember(Message.Author, Message.Channel.Guild)))
			{
				await Message.RespondAsync("Only admins can use WIP commands");
			}

			var embed = new DiscordEmbedBuilder()
			{
				Title = "JJL77",
				ThumbnailUrl = "http://cdn.edgecast.steamstatic.com/steamcommunity/public/images/avatars/c0/c0906ab7b6c889fdd49aef445e2d3f822a87ba59_medium.jpg",
			};
			embed.AddField("Level", "21");
			embed.AddField("Join Date", "April 1, 2014");
			embed.AddField("Games Owned", "69");
			embed.AddField("Friends", "50");
			await Message.RespondAsync(embed: embed);
		}
	}
}
