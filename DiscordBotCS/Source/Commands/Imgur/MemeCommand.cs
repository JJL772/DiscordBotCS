﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using Imgur.API.Models;
using Imgur.API.Models.Impl;

namespace DiscordBotCS.Commands.Imgur
{
	public class MemeCommand : BaseCommand
	{
		public MemeCommand()
		{
			Aliases = new List<string>();
			Aliases.Add("$meme");
			Aliases.Add("$randmeme");
			Aliases.Add("$randomeme");
			HelpText = "Displays a random meme";
		}

		public override async void Run(string[] args, DiscordMessage msg, DiscordBot bot)
		{
			List<IGalleryItem> items = (await bot.GalleryEndpoint.GetMemesSubGalleryAsync()).ToList();
			GalleryItem it = (GalleryItem)items[bot.RandomEngine.Next(0, items.Count - 1)];
			if (typeof(GalleryImage).IsAssignableFrom(it.GetType()))
			{
				GalleryImage img = (GalleryImage)it;
				await msg.RespondAsync(embed: new DiscordEmbedBuilder()
				{
					Title = img.Title,
					ImageUrl = img.Link,

				});
			}
			else if(typeof(GalleryAlbum).IsAssignableFrom(it.GetType()))
			{
				List<IImage> imgs = ((GalleryAlbum)it).Images.ToList();
				IImage img = imgs[bot.RandomEngine.Next(0, imgs.Count - 1)];
				await msg.RespondAsync(embed: new DiscordEmbedBuilder()
				{
					Title = img.Title,
					ImageUrl = img.Link,
				});
			}
			else
			{
				await msg.RespondAsync("Whoops! Something went very wrong :(((((   (you're fucked)");
			}

		}
	}
}
