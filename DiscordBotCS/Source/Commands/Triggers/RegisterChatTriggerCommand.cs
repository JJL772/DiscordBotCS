﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;

namespace DiscordBotCS.Commands.Triggers
{
	public class RegisterChatTriggerCommand : BaseCommand
	{
		public RegisterChatTriggerCommand()
		{
			Aliases = new List<string>();
			Aliases.Add("$addtrigger");
			Aliases.Add("$regtrigger"); 
			Aliases.Add("$registertrigger");
			HelpText = "Registers a chat trigger. The syntax for this is as follows: \n" +
				"$addtrigger OnUserChat 'Hello %<User>%!'";
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{

		}
	}
}
