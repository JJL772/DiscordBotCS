﻿using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiscordBotCS.Commands
{
	public class ArchiveCommand : BaseCommand
	{
		public ArchiveCommand()
		{
			Aliases.Add("$archive");
			HelpText = "Archives a bunch of messages in a channel. Syntax: $archive startdate=MM-DD-YYYY enddate=MM-DD-YYYY maxmessages=1000";
			IsHidden = false;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			
		}
	}
}
