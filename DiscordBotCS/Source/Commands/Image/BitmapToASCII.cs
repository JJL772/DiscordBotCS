﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Bot;
using DSharpPlus.Entities;
using System.Net;
using System.Drawing;
using System.Drawing.Imaging;
using Converter = DiscordBotCS.Util.ImageProcessing.BitmapToASCII;
using DiscordBotCS.Util.ImageProcessing;
using System.IO;

namespace DiscordBotCS.Commands.Image
{
	public class BitmapToASCII : BaseCommand
	{
		public BitmapToASCII()
		{
			Aliases.Add("$ascii");
			HelpText = "Converts an image to an ASCII formatted text file";
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			List<DiscordAttachment> attachments = Message.Attachments.ToList();
			if(attachments.Count == 0)
			{
				await Message.RespondAsync("To use this command, attach a valid image file to your message containing $ascii");
				return;
			}
			else if(attachments.Count > 1)
			{
				await Message.RespondAsync("Please attach only one file at a time!");
				return;
			}

			foreach (DiscordAttachment att in attachments)
			{
				if (att.Height <= 0 || att.Width <= 0)
				{
					await Message.RespondAsync("An error occurred whilst processing the file " + att.FileName + "\n" +
						"```\n*The specified file is either an empty image, or a non image file.\nNote: Please upload only PNG, JPG or BMP images.```");
					break;
				}
				else if (att.FileSize > 8000000)
				{
					await Message.RespondAsync("An error occurred whilst processing the file " + att.FileName + "\n" +
						"```\n*The specified file was too large\nNote: Please upload files smaller than 8Mb");
					break;
				}

				string FilePath = "Temp/" + att.FileName;

				byte[] data = bot.WebClient.DownloadData(att.Url);

				Bitmap bmp = (Bitmap)Bitmap.FromStream(new MemoryStream(data));

				if(bmp.Width <= 0 || bmp.Height <= 0)
				{
					await Message.RespondAsync("An error occurred whilst processing the file " + att.FileName + "\n" +
						"```\n*The specified file has a width of 0 or height of 0.\nNote: To fix, input a valid file.");
					break;
				}

				if(bmp.PixelFormat == PixelFormat.Indexed || bmp.PixelFormat == PixelFormat.Gdi)
				{
					await Message.RespondAsync("An error occurred whilst processing the file " + att.FileName + "\n" +
						"```\n*The specified file is a grayscale image, or lacks R, G and B channels.\nNote: To fix, import to an image editing program, and export with all three RGB channels");
					break;
				}
				else if(bmp.PixelFormat == PixelFormat.Undefined)
				{
					await Message.RespondAsync("An error occurred whilst processing the file " + att.FileName + "\n" +
						"```\n*The specified file has an unspecified pixel format\nNote: To fix, try to import and export the image from an image editor");
					break;
				}
				else if(bmp.PixelFormat == PixelFormat.Max || bmp.PixelFormat == PixelFormat.Gdi || bmp.PixelFormat == PixelFormat.PAlpha || bmp.PixelFormat == PixelFormat.Extended ||
					bmp.PixelFormat == PixelFormat.Canonical || bmp.PixelFormat == PixelFormat.Alpha)
				{
					await Message.RespondAsync("An error occurred whilst processing the file " + att.FileName + "\n" +
						"```\n*The specified file has an invalid pixel format\nNote: To fix, try importing and exporting the image from an image editor");
					break;
				}
				else if(bmp.PixelFormat == PixelFormat.DontCare)
				{
					await Message.RespondAsync("An error occurred whilst processing the file " + att.FileName + "\n" +
						"```\n*The specified file is acting edgy. The pixel format is 'DontCare.' I'm not even joking.\nNote: To fix, take away it's minecraft and razor blades.");
					break;
				}

				await Message.RespondAsync("Processing image file...");
				Converter.ASCIIImage img = Converter.ConvertBitmapToASCII(bmp).GetValueOrDefault();

				MemoryStream f = new MemoryStream();
				
				for (uint y = 0; y < img.height; y++)
				{
					for (uint x = 0; x < img.width; x++)
					{
						
						f.WriteByte(Convert.ToByte(img.ImageContents[x, y]));
						if (x == img.width - 1)
							f.WriteByte(Convert.ToByte('\n'));
					}
				}
				f.Flush();
				f.Close();

				await Task.Delay(1000);
				await Message.RespondWithFileAsync(f, "result.txt", "Finished conversion of file!");
			}
		}
	}

	public class BitmapToHTMLCommand : BaseCommand
	{
		public BitmapToHTMLCommand()
		{
			Aliases.Add("$html");
			IsHidden = true;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			if (Message.Attachments == null)
			{
				await Message.RespondAsync("You must attach an image to convert it!");
				return;
			}

			foreach (DiscordAttachment att in Message.Attachments)
			{
				string Path = "Temp/" + att.FileName;
				bot.WebClient.DownloadFile(Message.Attachments[0].Url, Path);
				Bitmap img = (Bitmap)Bitmap.FromFile(Path);
				if (img.PixelFormat == PixelFormat.Canonical || img.PixelFormat == PixelFormat.Extended || img.PixelFormat == PixelFormat.DontCare || img.PixelFormat == PixelFormat.Undefined ||
					img.PixelFormat == PixelFormat.Indexed || img.PixelFormat == PixelFormat.Gdi || img.PixelFormat == PixelFormat.Max)
				{
					await Message.RespondAsync("Unable to convert image! Invalid format!");
					return;
				}

				await Message.RespondAsync("Converting image file... (This may take a while)");
				HTMLImage? i = BitmapToHTML.ConvertBitmapToHTML(img);
				if (i == null)
				{
					await Message.RespondAsync("Unable to convert image! Unspecified error.");
					return;
				}

				HTMLImage g = i.GetValueOrDefault();
				StreamWriter f = new StreamWriter("Temp/out.html");
				foreach (char c in g.Data)
				{
					f.Write(c);
				}
				f.Flush();
				f.Close();
				await Message.RespondWithFileAsync("Temp/out.html", "Finished converting " + att.FileName + "!");

				File.Delete("Temp/out.html");
				File.Delete(Path);
			}
		}
	}
}
