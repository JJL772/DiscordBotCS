﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DiscordBotCS.Util;

namespace DiscordBotCS.Commands.Attributes
{
	[AttributeUsage(AttributeTargets.Class)]
	class Command : Attribute
	{
		public string Name { get; set; }

		public Command()
		{

		}
	}
}
