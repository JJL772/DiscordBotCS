﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DSharpPlus;
using DSharpPlus.Entities;
using DiscordBotCS.Bot;
using DiscordBotCS;

namespace DiscordBotCS.Commands
{
	public class HelpCommand : BaseCommand
	{
		public HelpCommand()
		{
			Aliases = new List<String>();
			Aliases.Add("$help");
			Aliases.Add("$h");
			HelpText = "Displays this help dialog";
			UseMultithreading = true;
		}

		public override async void Run(string[] Args, DiscordMessage Message, DiscordBot bot)
		{
			var embed = new DiscordEmbedBuilder();
			var emote = DiscordEmoji.FromName(Application.Bot.Client, ":question:");
			embed.Title = $"Help {emote}";
			foreach(BaseCommand c in Application.Bot.Commands.Commands)
			{
				string cmds = "";
				string helpText = c.HelpText == "" ? "None." : c.HelpText;
				foreach(string alias in c.Aliases)
				{
					cmds += alias + ", ";
				}
				if (!c.IsHidden)
					embed.AddField(cmds, helpText);	
			}
			await Message.RespondAsync(embed: embed);
		}
	}
}
