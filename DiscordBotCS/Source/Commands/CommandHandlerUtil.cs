﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using DSharpPlus.Entities;
using DiscordBotCS.Bot;
namespace DiscordBotCS.Commands
{
	public static class CommandWrapperUtil
	{
		public static async Task RunCommand(Action<string[], DiscordMessage, DiscordBot> fnc, string[] arg, DiscordMessage msg, DiscordBot bot)
		{
			fnc(arg, msg, bot);
		}
	}
}
